/**
 * Created by estratek on 14/03/18.
 */
var path = require('path');
var extractPlugin = require('extract-text-webpack-plugin');
var CSSExtract = new extractPlugin('styles.css');
module.exports = {
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'index.js',
        libraryTarget: "commonjs"
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                include: path.resolve(__dirname, 'src'),
                exclude: /(node_modules|bower_components|build)/,
                loader: 'babel-loader'
            },
            {
                test: /\.css$/,
                use: extractPlugin.extract({
                    use: 'css-loader'
                })
            }
        ]
    },
    plugins: [new extractPlugin('_style.css')],
    externals: {
        react: 'react',
        axios : 'axios',
        moment:'moment',
        reactstrap:'reactstrap'
    }

};
var axios = require('axios');
// globalVars 
let onDevMap, ignoreMicroServices, services;
const returnGlobalVars = () => {
    try {
        if (!!GLOBAL_VARS) {
            onDevMap=GLOBAL_VARS.onDevMap;
            ignoreMicroServices=GLOBAL_VARS.ignoreMicroServices;
            services=GLOBAL_VARS.mapServers;
        } else {
            onDevMap=false;
            ignoreMicroServices=false;
            services={};
        }

    } catch (e) {
        onDevMap=false;
        ignoreMicroServices=false;
        services={};
    }
    
}
export function getPrimaryKeyOfStructure(_structure){
    return Object.keys(_structure).find(x=>!!_structure[x].primaryKey)
}
// console.log('GLOBAL VARS',GLOBAL_VARS);
export function getUrlApi(_tipo,_props){
    returnGlobalVars()
    let microservice=(!!_props.microservice && !ignoreMicroServices)?('/'+_props.microservice):'';
    let host=reMapHost(_props.microservice,services) || ((!onDevMap)?_props.host:null) ||'';
    let model="/"+(_props.model);
    switch (_tipo){
        case 'structure':
            return host+microservice+'/structure'+model;
        default:
            return host+microservice+model;
    }
}
const reMapHost=function (_microservice,_objectMap) {
    returnGlobalVars()
    if (!onDevMap) return null;
    if (!_microservice || !_objectMap ) return _microservice;
    for (let value in _objectMap){
        if (value==_microservice) return _objectMap[value].host;
    }
    return '';
};

export const deleteItem = (host,microservice,model,_id, message) => {
    returnGlobalVars()
    const _message = message || `Seguro desea eliminar el elemento con ID: ${_id}`;
    if (confirm(_message)){
        // console.log('getURLAPI', getUrlApi('',{microservice,host,model}))
        return axios.delete(getUrlApi('',{microservice,host,model})+'/'+_id)
    }
}

var React = require('react');
var axios = require('axios');
import { ClipLoader } from 'react-spinners';

//

export default class UploadMyFile extends React.Component {
    constructor(){
        super();
        this.state={valueFile:'', loading: false};
    }

    componentWillReceiveProps(nextProps){
        this.setState(() => ({ loading: !nextProps.flagOfRenderIMG }))
    }

    handleUploadFile = async (event) => {
        this.props.renderIMG(false)
        this.setState(() => ({ loading: true }))
        // console.log('cambio',event);
        let self=this;
        await this.uploadFileAjax(event.target.files[0]).then((res)=>self.props.onUploaded(res));
        this.setState(() => ({ loading: false }))
    };

    uploadFileAjax = async (file) => {
        const data = new FormData();
        data.append('file', file);
        return axios.post('https://apipersona.estratek.com/upload/uploadFile', data);
    }
    tryClick(){
        //this.simulateClick();
        this.inputElement.click();
    }
    render(){
        return (
            <div>
                <div onClick={this.tryClick.bind(this)}>
                {this.state.loading ? <div className="stk-Forms-loading"> <ClipLoader width={50} /> </div> : this.props.elementToShow}
                </div>
                <input type="file"
                       onChange={this.handleUploadFile.bind(this)}
                       ref={input=>this.inputElement= input}
                       hidden
                       value={this.state.valueFile}/>
            </div>
        )
    }
}
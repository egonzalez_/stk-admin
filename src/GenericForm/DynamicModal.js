/**
 * Created by estratek on 15/12/17.
 */
import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import GenericFormApi from './GenericFormApi';
import ModalError from './ModalError';
import { Modal } from 'reactstrap';
import { BarLoader } from 'react-spinners';

import { getUrlApi, getPrimaryKeyOfStructure } from './utils';

class DynamicModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      formState: null,
      valueForm: {},
      formHasError: false,
      loading: false,
      errorOnSave: false,
      saving: false,
      showModalError: false,
      errorServerMessage: null,
      showingDoneMessage: false,
      disabledWhenUploading: false
    };
    this.myRef = React.createRef();
  }
  componentDidMount = () => {
    this.setState({ showingDoneMessage: false });
  };

  onClose = e => {
    this.props.onClose(e);
    this.setState({ formState: null, valueForm: {}, formHasError: false });
  };
  onChange = e => {
    let changes = JSON.parse(JSON.stringify(e));
    this.setState(prevState => {
      return {
        formState: changes.form,
        formHasError: changes.hasError,
        valueForm: changes,
        structure: changes.structure
      };
    });
    // this.props.onChange(changes)
  };
  hideModal = () => {
    this.setState(
      {
        formState: null,
        valueForm: {},
        formHasError: false,
        loading: false,
        errorOnSave: false,
        saving: false,
        showModalError: false,
        errorServerMessage: ''
      },
      this.onClose
    );
  };
  getUrlAPI(_tipo) {
    return getUrlApi(_tipo, this.props);
  }
  save = async () => {
    let formState;

    try {
      formState = await this.props.beforeSave(this.state.formState);

      if (this.props.fakeSave) {
        this.props.onSave(formState);
        return this.hideModal();
      }
      this.setState({ loading: true });
      // console.log('form state', this.props.saveRelationsAfterSave);
      if (this.props.saveRelationsAfterSave) {
        // console.log('FORM STATE', formState);
        formState = this.removeDependenciesRelations(formState);
        // console.log('FORM STATE', formState);
      }
      let res = [];
      if (this.props.axiosInstance) {
          console.log('hago esto')
        res = await this.props.axiosInstance.post(this.getUrlAPI(), formState);
      } else {
        res = await axios.post(this.getUrlAPI(), formState);
      }
      if (this.props.saveRelationsAfterSave) {
        // console.log('pruebas pruebas');
        await this.makeDependenciesRows(
          res.data[getPrimaryKeyOfStructure(this.state.structure)]
        );
      }
      this.setState({ loading: false }, async e => {
        await this.showDoneMessage();
        this.props.onSave(res.data);
        this.hideModal();
        // await this.getSnapshotBeforeUpdate()
      });
    } catch (e) {
      console.log('error', e.response);
      this.setState({
        errorOnSave: true,
        loading: false,
        showModalError: true,
        errorServerMessage: e
      });
      console.log('form', formState);
    }
  };
  removeDependenciesRelations = _values => {
    let valuesObj = JSON.parse(JSON.stringify(_values));
    let structure = JSON.parse(JSON.stringify(this.state.structure));
    let newStructure = {};
    Object.keys(valuesObj).map(x => {
      if (!(structure[x].ref || structure[x].subtype == 'dynamictable')) {
        newStructure[x] = valuesObj[x];
      }
    });
    return newStructure;
  };

  getDependenciesRelations = () => {
    let structure = JSON.parse(JSON.stringify(this.state.structure));
    let relationsObjects = [];
    //FILTER OBJS OF STRUCTURE THAT ARE DYNAMICTABLE AND ARE NOTE HIDDEN
    Object.keys(structure).map(_x => {
      if (
        !structure[_x].hidden &&
        (!!structure[_x].ref ||
          (structure[_x].subtype && structure[_x].subtype == 'dynamictable'))
      ) {
        relationsObjects.push({
          real_name: _x,
          ...structure[_x]
        });
      }
    });
    return relationsObjects;
  };
  makeDependenciesRows = async _viaValue => {
    let dependencies = this.getDependenciesRelations(_viaValue);
    let listRequest = [];
    dependencies.map(dep => {
      if (
        this.state.formState[dep.ref] &&
        Array.isArray(this.state.formState[dep.ref])
      ) {
        this.state.formState[dep.ref].map(_x => {
          _x[dep.via] = _viaValue;
          listRequest.push({
            config: {
              model: dep.ref,
              microservice: dep.microservice,
              host: this.props.host
            },
            data: JSON.parse(JSON.stringify(_x))
          });
        });
      }
    });
    await this.saveDependenciesRows(listRequest);
  };
  saveDependenciesRows = async _listRequest => {
    for (let value in _listRequest) {
      try {
        let res = await axios.post(
          getUrlApi(null, _listRequest[value].config),
          _listRequest[value].data
        );
      } catch (e) {
        console.error('GFROMS.DynamicModal: saving dependence error', e);
      }
    }
  };
  update = async () => {
    let formState = this.props.beforeUpdate(this.state.formState);
    if (this.props.fakeSave) {
      this.props.onUpdate(formState);
      return this.hideModal();
    }
    this.setState({ loading: true });
    axios
      .put(this.getUrlAPI() + '/' + this.props.id, formState)
      .then(async res => {
        this.setState({ loading: false }, async () => {
          this.showDoneMessageCallBack(() => {
            //console.log('hola mundo',res.data);
            this.props.onUpdate(res.data);
            this.hideModal();
          });
        });
      })
      .catch(e => {
        this.setState({
          errorOnSave: true,
          loading: false,
          errorServerMessage: e
        });
      });
  };
  delete = () => {
    if (this.props.fakeSave || !this.props.id) {
      if (!!this.props.onDelete) {
        this.props.onDelete(this.state.formState);
      }
      return this.hideModal();
    }
    this.setState({ loading: true });
    axios
      .delete(this.getUrlAPI() + '/' + this.props.id)
      .then(res => {
        this.setState({ loading: false });
        if (!!this.props.onDelete) {
          this.props.onDelete(res.data);
        }
        this.hideModal();
      })
      .catch(e => {
        this.setState({
          errorOnSave: true,
          loading: false,
          errorServerMessage: e
        });
      });
  };
  componentWillUnmount = () => {
    // console.log('unmount', this.state);
  };

  showDoneMessage = async _ms => {
    if (!_ms) _ms = this.props.showDoneMessageSeconds;
    this.setState({ showingDoneMessage: true });
    await this.timeout(_ms);
    this.setState({ showingDoneMessage: false });
    // console.log('END', new Date())
  };

  showDoneMessageCallBack = (_callback, _ms) => {
    if (!_ms) _ms = this.props.showDoneMessageSeconds;
    this.setState({ showingDoneMessage: true }, () => {
      setTimeout(() => {
        this.setState({ showingDoneMessage: false }, () => {
          // console.log('changed data',this.state);
          _callback();
        });
      }, _ms);
    });
  };

  timeout = ms => {
    return new Promise(resolve => setTimeout(resolve, ms));
  };

  disabledButtonWhenUploading = flag => {
    this.setState(() => ({ disabledWhenUploading: flag }));
  };

  render() {
    // console.log('SHOWINDG DONE MESSAGE',this.state.showingDoneMessage);
    return (
      <Modal isOpen={this.props.isOpen} size="lg" tabIndex="0" ref={this.myRef}>
        <ModalError
          value={this.state.errorServerMessage}
          onAccept={e => {
            this.setState({ errorServerMessage: null });
          }}
        />
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title">{this.props.title}</h5>
            <button type="button" className="close" onClick={this.onClose}>
              <span>&times;</span>
            </button>
          </div>
          {this.state.showingDoneMessage && (
            <div className="modal-body text-center">
              <h4>
                <i className="fa fa-check text-success" aria-hidden="true" />
              </h4>
              <h4 className="text-center text-success">Listo!</h4>
            </div>
          )}
          {!this.state.showingDoneMessage && (
            <React.Fragment>
              <div className="modal-body">
                {this.state.loading && (
                  <div className="stk-Forms-loading">
                    {' '}
                    <BarLoader width={100} />{' '}
                  </div>
                )}
                <GenericFormApi
                  host={this.props.host}
                  microservice={this.props.microservice}
                  model={this.props.model}
                  readOnly={this.state.loading || this.props.readOnly}
                  id={this.props.id}
                  value={this.state.valueForm}
                  rawData={this.props.rawData}
                  onChange={this.onChange}
                  transformStructure={this.props.transformStructure}
                  title={this.props.model}
                  disabledButtonWhenUploading={this.disabledButtonWhenUploading}
                  axiosInstance={this.props.axiosInstance}
                />
              </div>
              {this.state.loading && (
                <div className="modal-footer ">
                  <button
                    type="button"
                    className="btn btn-default text-uppercase"
                    disabled
                  >
                    <i className="fa fa-spinner fa-pulse fa-fw" /> Loading
                  </button>
                </div>
              )}

              {!this.state.loading && (
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-link text-uppercase"
                    onClick={this.onClose}
                  >
                    Cancelar
                  </button>

                  {!this.props.id && !this.props.readOnly && (
                    <button
                      type="button"
                      className="btn btn-default text-uppercase"
                      disabled={this.state.formHasError}
                      onClick={this.save}
                    >
                      Guardar
                    </button>
                  )}

                  {!!this.props.id &&
                    !this.props.readOnly &&
                    !this.props.hideDeleteButton && (
                      <button
                        type="button"
                        className="btn btn-danger text-uppercase"
                        onClick={this.delete}
                      >
                        Eliminar
                      </button>
                    )}

                  {!!this.props.id && !this.props.readOnly && (
                    <button
                      type="button"
                      className="btn btn-default text-uppercase"
                      disabled={
                        this.state.disabledWhenUploading ||
                        this.props.disableUpdateButton ||
                        (this.state.formHasError || !this.state.formState)
                      }
                      onClick={this.update}
                    >
                      Actualizar
                    </button>
                  )}
                </div>
              )}
            </React.Fragment>
          )}
        </div>
      </Modal>
    );
  }
}

DynamicModal.propTypes = {
  model: PropTypes.string,
  host: PropTypes.string.isRequired,
  microservice: PropTypes.string,
  isOpen: PropTypes.bool,
  id: PropTypes.string,
  transformStructure: PropTypes.func,
  onUpdate: PropTypes.func,
  onSave: PropTypes.func,
  onDelete: PropTypes.func,
  saveRelationsAfterSave: PropTypes.bool,
  fakeSave: PropTypes.bool,
  hideDeleteButton: PropTypes.bool,
  showDoneMessageSeconds: PropTypes.number,
  axiosInstance: PropTypes.instanceOf(axios)
};
DynamicModal.defaultProps = {
  model: '',
  onSave: () => {},
  onClose: () => {},
  beforeSave: _structure => _structure,
  onUpdate: () => {},
  beforeUpdate: _structure => _structure,
  onDelete: () => {},
  transformStructure: _structure => _structure,
  fakeSave: false,
  saveRelationsAfterSave: false,
  closeOnEsc: false,
  hideDeleteButton: true,
  showDoneMessageSeconds: 900
};

export default DynamicModal;

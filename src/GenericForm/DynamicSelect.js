/**
 * Created by estratek on 15/12/17.
 */
var React = require("react");
import GfsValidator, { getArrayErrors } from "./GfsValidator";
import Select from "react-select";
import { Select as SelectPlus } from "react-select-plus";
var axios = require("axios");
import DynamicModal from "./DynamicModal";
import GfsDescriptionPopOver from "./GfsDescriptionPopOver";
import GfsOnlyView from "./GfsOnlyView";
import { getUrlApi } from "./utils";
import { BarLoader } from "react-spinners";
import PropTypes from "prop-types";

class DynamicSelect extends React.Component {
  state = {
    value: this.props.value,
    options: [],
    showModal: false,
    showModalEdit: false,
    showModalCreate: false,
    loadingData: false,
    errorLoading: false,
    readOnlyData: ""
  };
  canEdit(value) {
    return !!value && !(typeof value != "object");
  }
  componentWillUpdate(nextProps) {
    if (nextProps.value !== this.props.value) {
      let newErrorState =
        getArrayErrors(nextProps.validation, nextProps.value).length > 0;
      this.props.onChangeError(newErrorState);
    }
  }
  componentDidUpdate = (prevProps, prevState) => {
    //console.log('PARAMS',prevProps.paramsApi,this.props.paramsApi);
    if (
      JSON.stringify(prevProps.paramsApi) !=
      JSON.stringify(this.props.paramsApi)
    ) {
      this.getData();
    }
  };

  componentDidMount() {
    if (this.props.readOnly) {
      return this.getReadOnlyData();
    }
    return this.getData();
  }
  getUrlAPI(_tipo) {
    return getUrlApi(_tipo, this.props);
  }
  getData() {
    this.setState({ loadingData: true });
    let buttonAdd = this.props.describerComponent;
    if (this.props.axiosInstance) {
      if(buttonAdd == false){
        this.props.axiosInstance
          .get(this.getUrlAPI()+'?limit=20000', { params: { ...this.props.paramsApi } })
          .then(({ data }) => {
            this.setState({ options: data, loadingData: false });
          })
          .catch(err => {
            this.setState({ errorLoading: true });
          });
      }else{
        this.props.axiosInstance
          .get(this.getUrlAPI(), { params: { ...this.props.paramsApi } })
          .then(({ data }) => {
            this.setState({ options: data, loadingData: false });
          })
          .catch(err => {
            this.setState({ errorLoading: true });
          });
      }
    } else {
      axios
        .get(this.getUrlAPI(), { params: { ...this.props.paramsApi } })
        .then(({ data }) => {
          this.setState({ options: data, loadingData: false });
        })
        .catch(err => {
          this.setState({ errorLoading: true });
        });
    }
  }
  getReadOnlyData() {
    this.setState({ loadingData: true });
    // If other instance of axios is send it thru other Top Component
    if (this.props.axiosInstance) {
      this.props.axiosInstance
        .get(`${this.getUrlAPI()}/${this.props.value}`)
        .then(({ data }) =>
          this.setState(prevState => {
            //console.log('DATA OBTENIDA', data, this.props);
            return { readOnlyData: data, loadingData: false };
          })
        )
        .catch(err => {
          this.setState({ errorLoading: true });
        });
    } else {
      axios
        .get(`${this.getUrlAPI()}/${this.props.value}`)
        .then(({ data }) =>
          this.setState(prevState => {
            //console.log('DATA OBTENIDA', data, this.props);
            return { readOnlyData: data, loadingData: false };
          })
        )
        .catch(err => {
          this.setState({ errorLoading: true });
        });
    }
  }
  handleChange(event) {
    if (!!event) {
      if (event instanceof Array) {
        let newArray = event.concat(event[this.props.valueKey]);
        this.props.onChange(event);
        this.setState({ value: event });
      } else {
        let newArray = event[this.props.valueKey];
        this.props.onChange(newArray);
        this.setState({ value: newArray });
      }
    } else {
      this.props.onChange("");
      this.setState({ value: "" });
    }
  }
  showModalCreate() {
    this.setState({ showModalCreate: true });
  }
  onSave = newData => {
    this.setState(
      {
        options: this.state.options.concat(newData),
        showModalEdit: false,
        showModalCreate: false
      },
      () => this.handleChange(newData)
    );
  };
  onDelete(deleteData) {
    this.setState({
      showModalEdit: false,
      showModalCreate: false,
      options: this.state.options.filter((currentValue, index) => {
        return (
          currentValue[this.props.valueKey] != deleteData[this.props.valueKey]
        );
      })
    });
  }
  onUpdate(newData) {
    var newArray = [];
    var options = this.state.options;
    for (var value in options) {
      if (
        this.state.options[value][this.props.valueKey] !=
        newData[this.props.valueKey]
      ) {
        newArray[value] = this.state.options[value];
      } else {
        newArray[value] = newData;
      }
    }
    this.setState({ options: newArray });
  }
  showModalEdit() {
    if (!this.props.value) {
      return false;
    }
    this.setState({ showModalEdit: true });
  }

  hasError() {
    return getArrayErrors(this.props.validation, this.props.value).length > 0;
  }
  capitalizeLabelOnStructure = str => {
    let _str = {}
    Object.keys(str).map(el => {
      _str = {
        ..._str,
        [el]: {
          ...str[el],
          label: str[el].label
            ? str[el].label.charAt(0).toUpperCase() + str[el].label.slice(1)
            : undefined
        }
      };
    });
    return _str;
  }
  render() {
    if (this.props.readOnly) {
      if (this.state.readOnlyData) {
        return (
          <GfsOnlyView
            label={this.props.label}
            valueString={this.state.readOnlyData[this.props.labelKey]}
          />
        );
      }
      return (
        <div>
          {this.state.loadingData && <BarLoader height={1} width="100%" />}
          <GfsOnlyView label={this.props.label} />
        </div>
      );
    }
    return (
      <div
        className={
          (!this.props.classWrapp ? "form-group" : this.props.classWrapp) +
          " " +
          (this.hasError() ? "has-error" : "")
        }
      >
        <DynamicModal
          isOpen={this.state.showModalCreate}
          model={this.props.model}
          onSave={this.onSave}
          microservice={this.props.microservice}
          onClose={() => this.setState({ showModalCreate: false })}
          host={this.props.host}
          title={this.props.label|| this.props.model}
          axiosInstance={this.props.axiosInstance}
          transformStructure={this.capitalizeLabelOnStructure}
        />
        <DynamicModal
          isOpen={this.state.showModalEdit}
          model={this.props.model}
          onSave={this.onSave}
          onUpdate={this.onUpdate.bind(this)}
          onDelete={this.onDelete.bind(this)}
          hideDeleteButton={this.props.hideDeleteButton}
          onClose={() => this.setState({ showModalEdit: false })}
          microservice={this.props.microservice}
          id={this.props.value}
          host={this.props.host}
          title={'Editar ' + this.props.label|| this.props.model}
          axiosInstance={this.props.axiosInstance}
          transformStructure={this.capitalizeLabelOnStructure}
        />

        {!!this.props.label && (
          <label className="form-control-label mb-1 ">
            {" "}
            <strong> {this.props.label}</strong>
            {this.props.description && (
              <GfsDescriptionPopOver description={this.props.description} />
            )}
          </label>
        )}
        <div className="input-button-Actions f-right">
          {!this.props.hideAddButton && (
            <button
              className="btn btn-link btn-sm"
              onClick={this.showModalCreate.bind(this)}
            >
              Agregar
            </button>
          )}
          {(!this.props.hideUpdateButton) && (
            this.props.value && <button
              className="btn btn-link btn-sm"
              onClick={this.showModalEdit.bind(this)}
              disabled={this.canEdit(this.props.value)}
            >
              Editar
            </button>
          )}
        </div>
        {this.props.hasGroups && (
          <SelectPlus
            disabled={
              this.props.disabled ||
              this.state.loadingData ||
              this.state.errorLoading
            }
            value={this.props.value}
            onChange={this.handleChange.bind(this)}
            labelKey={this.props.labelKey}
            valueKey={this.props.valueKey}
            placeholder={this.props.placeHolder}
            multi={this.props.multiple}
            options={this.props.options || this.state.options}
          />
        )}
        {!this.props.hasGroups && (
          <Select
            disabled={
              this.props.disabled ||
              this.state.loadingData ||
              this.state.errorLoading
            }
            value={this.props.value}
            onChange={this.handleChange.bind(this)}
            labelKey={this.props.labelKey}
            valueKey={this.props.valueKey}
            placeholder={this.props.placeHolder}
            multi={this.props.multiple}
            options={this.props.options || this.state.options}
          />
        )}
        {this.state.loadingData && <BarLoader height={1} width="100%" />}
        {this.state.errorLoading && (
          <div style={{ color: "red" }}>Error obteniendo data</div>
        )}

        <GfsValidator
          validation={this.props.validation}
          value={this.props.value}
        />
      </div>
    );
  }
}

DynamicSelect.defaultProps = {
  onChangeError: () => {},
  onChange: () => {},
  onChangeMultipleArrayKeys: () => {},
  value: "",
  paramsApi: {}
};

DynamicSelect.propTypes = {
  disabled: PropTypes.bool,
  type: PropTypes.string,
  validation: PropTypes.object,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
  paramsApi: PropTypes.object,
  onChange: PropTypes.func,
  onChangeError: PropTypes.func,
  hideUpdateButton: PropTypes.bool,
  hideAddButton: PropTypes.bool,
  model: PropTypes.string,
  host: PropTypes.string,
  microservice: PropTypes.string,
  rawOption: PropTypes.array,
  valueKey: PropTypes.string.isRequired,
  labelKey: PropTypes.string.isRequired,
  description: PropTypes.string,
  placeHolder: PropTypes.string,
  hasError: PropTypes.bool,
  classWrapp: PropTypes.string,
  label: PropTypes.string,
  onChangeMultipleArrayKeys: PropTypes.func,
  axiosInstance: PropTypes.instanceOf(axios)
};
export default DynamicSelect;

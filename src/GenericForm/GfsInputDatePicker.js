/**
 * Created by estratek on 15/12/17.
 */
var React = require('react');
import GfsValidator,{getArrayErrors} from './GfsValidator'
import DatePicker, { registerLocale } from 'react-datepicker';
var moment= require('moment');
import PropTypes from 'prop-types';
import Spanish from 'date-fns/locale/es';
// registerLocale("es", Spanish);
import GfsOnlyView from './GfsOnlyView';
class GfsInputDatePicker extends React.Component {
    componentWillUpdate(nextProps){
        if (nextProps.value!==this.props.value && !!this.props.onChangeError){
            let newErrorState=getArrayErrors(nextProps.validation,nextProps.value).length>0;
            this.props.onChangeError(newErrorState);
        }
    }
    componentDidMount = () => {
        // moment.locale('es')
        registerLocale("es", Spanish);
    }
    
    handleChange=(event)=>{                    
        // let newVal=(new Date(event)).toISOString();
        // console.log('newVal', newVal)
        this.props.onChange(event);       
    }
    onChangeError(newState){
        if (newState != this.state.e_hasError){            
            this.props.onChangeError(newState);
        }
    }
    prepareValue=(_value)=>{
        return (!!_value)? _value:'';
    }
    hasError=()=> (
        !this.props.validation && getArrayErrors(this.props.validation,this.props.value).length>0
    )
    render () {
        if (this.props.readOnly){
            return (<GfsOnlyView label={this.props.label} valueString={(!!this.props.value)? moment(this.props.value).format('DD MMM. YYYY'):''}/>)
        }
        return (
            <div className={"form-group "+((this.hasError())?'has-error':'')}>
                {this.props.label && <label className="form-control-label"> <strong>{this.props.label}</strong></label>}
                <DatePicker                    
                    disabled={this.props.disabled}
                    className={"form-control "+((this.hasError())?'form-control-danger':'')}
                    placeholderText={this.props.placeHolder}
                    selected={this.prepareValue(this.props.value)}
                    startDate={this.prepareValue(this.props.startDate)}
                    endDate={this.prepareValue(this.props.endDate)}
                    selectsStart={this.props.selectsStart || false}
                    selectsEnd={this.props.selectsEnd || false}
                    onChange={this.handleChange}
                    minDate={(this.props.startDate && (new Date(this.props.startDate))) || ''}
                    maxDate={(this.props.endDate && (new Date(this.props.endDate))) || ''}
                    isClearable={true}
                    peekNextMonth
                    showMonthDropdown
                    showYearDropdown
                    dropdownMode="select"
                    dateFormatCalendar="MMMM"
                    locale="es"
                    fixedHeight
                    dateFormat="d/MM/YYYY"
                    // scrollableYearDropdown
                />
                <GfsValidator validation={this.props.validation}
                              value={this.props.value}/>
                {this.props.description && <small id="emailHelp" className="form-text text-muted">{this.props.description}</small>}
            </div>
        );
    }
}
GfsInputDatePicker.propTypes = {
    disabled: PropTypes.bool,
    type: PropTypes.string,
    validation: PropTypes.object,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    onChangeError: PropTypes.func,
    description: PropTypes.string,
    placeHolder: PropTypes.string,
    hasError: PropTypes.bool,
    label: PropTypes.string,
    readOnly: PropTypes.bool
};
GfsInputDatePicker.defaultProps = {
    disabled: false,
    type: 'text',
    validation: {},
    value: '',
    onChange: ()=>{},
    onChangeError: ()=>{},
    description: "",
    placeHolder: "",
    hasError: false,
    label: "",
    readOnly:false
};

export default  GfsInputDatePicker;
import React, { Component } from 'react'
import PropTypes from 'prop-types'
var Modal = require('reactstrap').Modal;

class ModalError extends Component {
    state = {
        isOpen: true
    }


    render() {
        return (
            <div>
                <Modal isOpen={this.props.value}>
                    <div className="modal-header">
                        ERROR
              </div>
                    <div className="modal-body row">
                        {this.props.value && this.props.value.response && this.props.value.response.data && (this.props.value.response.data.error || this.props.value.response.data.message) && (this.props.value.response.data.message || this.props.value.response.data.error.message) &&
                            <React.Fragment>
                                <div className="col-md-2 text-center">
                                    <h5>
                                        <i className="fa fa-exclamation-circle f-50 text-danger" aria-hidden="true"></i>
                                    </h5>
                                </div>
                                <div className="col-md-10">
                                    <h5>
                                        { this.props.value.response.data.message || this.props.value.response.data.error.message}
                                    </h5>
                                </div>
                            </React.Fragment>
                        }
                    </div>
                    <div className="modal-footer">
                        <button className="btn btn-default" onClick={this.props.onAccept}>Aceptar</button>
                    </div>
                </Modal>
            </div>
        )
    }
}

export default ModalError;
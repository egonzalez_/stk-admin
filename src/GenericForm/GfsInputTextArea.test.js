/**
 * Created by estratek on 15/12/17.
 */
import React, {Component} from 'react';
import GfsInputTextArea   from './GfsInputTextArea';
import {shallow,mount} from 'enzyme';
import sinon from 'sinon';

describe('Gfs Input TextArea',()=>{
    it('it should render',()=>{
        const comp = shallow (<GfsInputTextArea value="50" />);
        expect(comp).toHaveLength(1);
    });
    it('it should render with value 50',()=>{
        const comp = shallow (<GfsInputTextArea value="50"  />);
        expect(comp.find('textarea').props().value).toBe("50");
    });
    it('it should call onChange after value change',()=>{
        const spy = sinon.spy();
        const wrapper = mount (<GfsInputTextArea value="hola"  onChange={spy}/>);
        expect(spy.calledOnce).toBe(false);
        wrapper.find('textarea').simulate('change', {target: {value: 'My new value'}});
        expect(spy.args[0][0]).toEqual('My new value');
    });
    it('it should change value when change prop value',()=>{
        const wrapper = shallow (<GfsInputTextArea value=""/>);
        expect(wrapper.find('textarea').props().value).toBe("");
        wrapper.setProps({value:'hola mundo'});
        expect(wrapper.find('textarea').props().value).toBe("hola mundo");
    });
    it('Validate min Length prop when initial prop',()=>{
        const spy = sinon.spy();
        const wrapper = shallow (<GfsInputTextArea value="test" onChangeError={spy}/>);
        expect(wrapper.find('textarea').props().value).toBe("test");
        expect(spy.calledOnce).toBe(true);
    });

    it('Check hasError with validationProp minLength',()=>{
        const spy = sinon.spy();
        const wrapper = shallow (<GfsInputTextArea value="" validation={{minLength:1}} onChangeError={spy}/>);
        expect(spy.called).toBe(true);
        expect(spy.args[spy.callCount-1][0]).toBe(true);
        /*check if validation is off*/
        wrapper.setProps({value:'hola mundo'});
        expect(spy.args[spy.callCount-1][0]).toEqual(false);
    });

    it('Check hasError with validationProp maxLength',()=>{
        const spy = sinon.spy();
        const wrapper = shallow (<GfsInputTextArea value="testing" validation={{maxLength:2}} onChangeError={spy}/>);
        expect(spy.called).toBe(true);
        expect(spy.args[spy.callCount-1][0]).toBe(true);
        /*check if validation is off*/
        wrapper.setProps({value:'te'});
        expect(spy.args[spy.callCount-1][0]).toEqual(false);
    });
    it('Check hasError with validationProp maxLength',()=>{
        const spy = sinon.spy();
        const wrapper = shallow (<GfsInputTextArea value="testing" validation={{maxLength:2}} onChangeError={spy}/>);
        expect(spy.called).toBe(true);
        expect(spy.args[spy.callCount-1][0]).toBe(true);
        /*check if validation is off*/
        wrapper.setProps({value:'te'});
        expect(spy.args[spy.callCount-1][0]).toEqual(false);
    });
    it('Check hasError with validationProp required',()=>{
        const spy = sinon.spy();
        const wrapper = shallow (<GfsInputTextArea value="" validation={{required:true}} onChangeError={spy}/>);
        expect(spy.called).toBe(true);
        expect(spy.args[spy.callCount-1][0]).toBe(true);
        /*check is required and is fullfilled*/
        wrapper.setProps({value:'te'});
        expect(spy.args[spy.callCount-1][0]).toEqual(false);
        /*check if is not required and input is empty*/
        wrapper.setProps({value:'',validation:{required:false}});
        expect(spy.args[spy.callCount-1][0]).toEqual(false);
    });
});

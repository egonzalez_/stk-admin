/**
 * Created by estratek on 15/12/17.
 */
var React = require('react');
import PropTypes from 'prop-types';
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap'


class GCard extends React.Component {

    state = {
        openDD: false
    }
    render() {
        return (

            <div className="card list-view-media GList ">
                <div className="card-block" style={{ paddingLeft: !this.props.img && '1.5em' }}>
                    <div className="media">
                        {this.props.img && <a className="media-center m-15">
                            {this.props.img &&
                                <img className="media-object card-list-img"
                                    style={{ height: '50px' }}
                                    src={this.props.img} alt="Generic placeholder image" />}
                        </a>}
                        <div className="media-body">
                            <div className="col-xs-12">
                                {this.props.title && <h6 className="d-inline-block title" onClick={e=>this.props.onClickTitle(this.props)}>{this.props.title}</h6>}
                                &nbsp;&nbsp;{this.props.titleLabel && <label className="label label-info"> {this.props.titleLabel} </label>}
                                <div className="editarbutton">
                                    <ButtonDropdown direction="left" isOpen={this.state.openDD} toggle={e => { this.setState({ openDD: !this.state.openDD }) }}>
                                        <DropdownToggle color="link">
                                            <i className="ti-more-alt" aria-hidden="true" />
                                        </DropdownToggle>
                                        <DropdownMenu >
                                            {!this.props.hideEditButton &&
                                                <DropdownItem type="button" title="" onClick={e => this.props.onClickEdit(this.props)} >
                                                    <span className="fa fa-edit text-default"></span> Editar
                                                </DropdownItem>
                                            }
                                            {!this.props.hideDeleteButton &&
                                                <DropdownItem type="button" title="" onClick={e => this.props.onClickDelete(this.props)} >
                                                    <span className="fa fa-trash text-danger"></span> Eliminar
                                                </DropdownItem>
                                            }
                                        </DropdownMenu>
                                    </ButtonDropdown>
                                </div>
                            </div>
                            {this.props.subtitle && <div className="f-13 text-muted m-b-5">{this.props.subtitle}</div>}
                            {this.props.paragraph && <p>{this.props.paragraph}</p>}
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}




GCard.propTypes = {
    img: PropTypes.string,
    title: PropTypes.string,
    titleLabel: PropTypes.string,
    subtitle: PropTypes.string,
    paragraph: PropTypes.string,
    onClickEdit: PropTypes.func,
    onClickDelete: PropTypes.func,
    realObj: PropTypes.object.require,
    hideDeleteButton: PropTypes.bool,
    hideEditButton: PropTypes.bool,
    hideAddButton: PropTypes.bool,
    onClickTitle:PropTypes.func
};
GCard.defaultProps = {
    disabled: false,
    type: 'text',
    validation: {},
    value: '',
    onChange: () => { },
    onChangeError: () => { },
    onClickTitle: () => { },
    description: "",
    placeHolder: "",
    hasError: false,
    label: "",
    readOnly: false,
    onClickEdit: () => { },
    onClickDelete: () => { },
    hideDeleteButton: false,
    hideEditButton: false,
    hideAddButton: false
};

export default GCard;
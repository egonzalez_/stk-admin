/**
 * Created by estratek on 15/12/17.
 */
var React = require('react');
import GfsValidator, { getArrayErrors } from './../GfsValidator'
import PropTypes from 'prop-types';
import GfsOnlyView from './../GfsOnlyView';
import DynamicModal from './../DynamicModal';
import GCard from './GCard'
import axios from 'axios'
import { BarLoader } from 'react-spinners';
import { getUrlApi, getPrimaryKeyOfStructure } from './../utils';


class GList extends React.Component {
    state = {
        structure: {},
        modalStructure: null,
        step: -1,
        limitItems: 5,
        hasMoreDataNext: true,
        hasMoreDataBack: false,
        loadingData: false,
        loadingStructure: false,
        showModalEdit: false,
        showModalAdd: false
    }
    componentDidMount() {

        this.getStructure()
    }
    getStructure() {
        this.setState({ loadingStructure: true });
        axios.get(this.getUrlAPI('structure')).then(({ data }) => {
            //let structureFetched = this.prepareStructureToPrint(JSON.parse(data.structure));
            let structure = this.props.transformStructure(JSON.parse(data.structure));
            this.setState({
                loadingStructure: false,
                structure: structure,
                structureSearch: structure
            }, () => this.steperData(1));

        }).catch((err) => {
            this.setState({ loadingStructure: false });
        });
    }
    getUrlAPI(_tipo) {
        return getUrlApi(_tipo, this.props);
    }
    steperData(_direction) {
        if (_direction > 0) {
            this.setState({ step: this.state.step + 1 }, () => this.getData());
        } else {
            if (this.state.step > 0) {
                this.setState({ step: this.state.step - 1 }, () => this.getData());
            }
        }
    }
    getData() {
        let params = {
            limit: this.state.limitItems,
            skip: this.state.limitItems * this.state.step,
        };
        if (!!this.props.viaRelation) {
            if (!this.props.viaValue) { return false }
            params[this.props.viaRelation] = this.props.viaValue;
        }
        this.setState({ loadingData: true });
        axios.get(this.getUrlAPI(),
            { params: params }
        ).then((res) => {

            this.setState({
                loadingData: false,
                hasMoreDataNext: res.data.length < this.state.limitItems
            }, () => this.props.onChange(res.data));
        }).catch((err) => {
            this.setState({ loadingData: false });
        });
    }
    transformParams = (_objRow) => {
        let propsToCard = {};
        Object.keys(this.props.mapStructure).map(x => {
            if ({}.toString.call(this.props.mapStructure[x]) === '[object Function]') {
                //if is a function add the result of the  function that has as parameters te row;
                propsToCard[x] = this.props.mapStructure[x](_objRow);
            } else if (_objRow[this.props.mapStructure[x]]) {
                propsToCard[x] = _objRow[this.props.mapStructure[x]];
            }
        })
        return propsToCard;
    }
    onUpdate = (obj, e) => {
        this.props.onChange(this.props.value.map((value) =>
            (obj == value) ? e : value
        ));
    }
    onDelete = (e) => {
        this.props.onChange(this.props.value.filter((value, index, array) => {
            return JSON.stringify(value) != JSON.stringify(e);
        }));
    }

    onSave = (e) => {
        this.setState({ showModalAdd: false, showModalEdit: false },
            this.props.onChange(
                this.props.value ? [e].concat(this.props.value) : [e]
            )
        );
    }
    deleteItem(e, keyPrimary) {
        if (!confirm('Desea eliminar este elemento?')) { return false }
        //if (!!this.props.fakeSave) { return this.onDelete(e); }
        this.setState({ loadingData: true });
        axios.delete(this.getUrlAPI() + '/' + e[keyPrimary]).then((res) => {
            this.setState({ loadingData: false });
            this.onDelete(e);
        }).catch((err) => {
            this.setState({ loadingData: false });
            console.log('error', err);
        })
    }
    capitalizeFirstLetter = (string) => {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
    onCloseAddModal = (_nose) => {
        this.setState({ showModalAdd: false })
    }

    transformStructureEdit = (_structure) => {
        let newStructure = JSON.parse(JSON.stringify(_structure))
        if (newStructure[this.props.viaRelation] && this.props.viaValue) {
            newStructure[this.props.viaRelation].value = this.props.viaValue
            newStructure[this.props.viaRelation].disabled = true
        }
        return newStructure;
    }
    transformStructureAdd = (_structure) => {
        let newStructure = JSON.parse(JSON.stringify(_structure))
        if (newStructure[this.props.viaRelation]) {
            newStructure[this.props.viaRelation].hidden = true;
            newStructure[this.props.viaRelation].required = false;
            if (this.props.viaValue) {
                newStructure[this.props.viaRelation].value = this.props.viaValue
            }
        }
        return newStructure;
    }
    showInfoModal = (e) => {s
        this.setState({ showModalEdit: true, showInfoModal: true, idEditingRow: e.realObj })
    }
    _hideDeleteButton = () => {
        if (typeof this.props.hideDeleteButton =='boolean'){
            return this.props.hideDeleteButton;
        }
        return this.state.structure._selfTablePermissions && this.state.structure._selfTablePermissions.hideDeleteButton
    }

    _hideEditButton = () => {
        if (typeof this.props.hideEditButton=='boolean'){
            return this.props.hideEditButton;
        }
        return this.state.structure._selfTablePermissions && this.state.structure._selfTablePermissions.hideEditButton
    }
    
    _hideAddButton = () => {
        
        if (typeof this.props.hideAddButton == 'boolean'){
            return this.props.hideAddButton;
        }
        return this.state.structure._selfTablePermissions && this.state.structure._selfTablePermissions.hideAddButton
    }
    

    render() {
        return (
            <React.Fragment>
                {this.state.idEditingRow && <DynamicModal
                    isOpen={this.state.showModalEdit}
                    model={this.props.model}
                    onUpdate={(e) => this.onUpdate(this.state.idEditingRow, e)}
                    onDelete={(e) => this.onDelete(this.state.idEditingRow)}
                    onClose={e => this.setState({ showModalEdit: false, showInfoModal: false, idEditingRow: null })}
                    //fakeSave={this.props.fakeSave}
                    id={this.state.idEditingRow[getPrimaryKeyOfStructure(this.state.structure)]}
                    microservice={this.props.microservice}
                    transformStructure={this.transformStructureEdit}
                    host={this.props.host}
                    readOnly={this.state.showInfoModal}
                    title={this.props.titleToEditModal || this.capitalizeFirstLetter(this.props.title)}
                />}
                <DynamicModal
                    isOpen={this.state.showModalAdd}
                    model={this.props.model}
                    onSave={this.onSave}
                    onClose={this.onCloseAddModal}
                    //fakeSave={this.props.saveRelationsAfterSave || this.props.fakeSave}
                    microservice={this.props.microservice}
                    host={this.props.host}
                    transformStructure={this.transformStructureAdd}
                    title={this.props.titleToAddModal || this.props.model}
                // title={this.props.model.search('detalle') === 0 || this.props.model.search('tipo') === 0 ? 'Nuevo Detalle ' + this.props.title : this.capitalizeFirstLetter(this.props.model)} 
                />
                <div className="card">
                    <div className="card-header">
                        <h5> <span>{this.props.title || this.props.model}</span></h5>
                        <div className="card-header-right"><i className="icofont icofont-spinner-alt-5"></i></div>
                        {!this._hideAddButton() && <button className="btn btn-link btn-sm pull-right" onClick={e => this.setState({ showModalAdd: true })}>
                            <i className="fa fa-plus"></i>
                        </button>}
                    </div>
                    {(this.state.loadingData || this.state.loadingStructure) && <div className="stk-Forms-loading"> <BarLoader width={100} /> </div>}
                    <div className="card-block">
                        <div className="row">
                            <div className="col-md-12">
                                <ul className="list-view">
                                    <li>
                                        {this.props.value.map(x => <GCard
                                            {...this.transformParams(x)}
                                            realObj={x}
                                            hideDeleteButton={this._hideDeleteButton()}
                                            hideEditButton={this._hideEditButton()}
                                            onClickDelete={e => this.deleteItem(x, getPrimaryKeyOfStructure(this.state.structure))}
                                            onClickTitle={e => this.showInfoModal(e)}
                                            onClickEdit={e => this.setState({ showModalEdit: true, idEditingRow: e.realObj })} />)}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    {!this.state.hasMoreDataNext && <button className="btn btn-link"
                        type="button"
                        id='ver_mas'
                        title="Ver mas"
                        onClick={() => this.steperData(1)}>
                        Ver Mas <i className="fa fa-plus" />
                    </button>}

                </div>
            </React.Fragment>
        );
    }
}




GList.propTypes = {
    disabled: PropTypes.bool,
    type: PropTypes.string,
    validation: PropTypes.object,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    onChangeError: PropTypes.func,
    description: PropTypes.string,
    placeHolder: PropTypes.string,
    hasError: PropTypes.bool,
    label: PropTypes.string,
    readOnly: PropTypes.bool,
    mapStructure: PropTypes.object,
    hideDeleteButton: PropTypes.bool,
    hideEditButton: PropTypes.bool,
    hideAddButton: PropTypes.bool,
};
GList.defaultProps = {
    disabled: false,
    type: 'text',
    validation: {},
    value: [],
    onChange: () => { },
    onChangeError: () => { },
    mapStructure: {},
    transformStructure: (e) => e,
    description: "",
    placeHolder: "",
    hasError: false,
    label: "",
    readOnly: false,
    hideDeleteButton: false,
    hideEditButton: false,
    hideAddButton: false,
    titleToAddModal: '',
    titleToEditModal: ''
};

export default GList;
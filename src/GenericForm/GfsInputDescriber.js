var React = require('react');
import PropTypes from 'prop-types';
var axios = require('axios');
import {getUrlApi} from './utils';

class GfsInputeDescriber extends React.Component {
    state={
        options:[],
        showOptions:false,
        loadingData:false,
        errorLoading:false
    }
    componentDidMount = () => {
        
        this.getData();
    }
    getData=()=>{
        this.setState({loadingData:true});
        axios.get(getUrlApi(null,this.props))
            .then(({data})=>{
                this.setState({options:data,loadingData:false})
            })
            .catch((err)=>{this.setState({errorLoading:true})});
    }
    handleChange=(_row)=>{
        this.props.onChange(_row[this.props.valueKey])
        this.setState({showOptions:false});
    }
    getValue=(_options,_valueKey,_value)=>{        
        let newVal= _options.find(value=>value[_valueKey]==_value);
        return newVal;
    }
    render() {
        return (
            <div className="input-group-btn">
                <button type="button" className="btn btn-secondary dropdown-toggle" 
                    onClick={()=>this.setState({showOptions:!this.state.showOptions})}>
                    {!!this.props.value 
                        && this.getValue(this.state.options,this.props.valueKey,this.props.value)
                        && this.getValue(this.state.options,this.props.valueKey,this.props.value)[this.props.labelKey]}
                </button>
                {this.state.showOptions && <div className="dropdown-menu show"
                    style={{"position":"absolute",transform:"translate3d(0px, 40px, 0px)",top:0,left:0,willChange:'transform'}}>
                    {this.state.options.map((_option,_key)=>
                        <a className="dropdown-item" 
                                onClick={(e)=>this.handleChange(_option)} key={_key}>{_option[this.props.labelKey]}</a>
                    )}
                </div>}                        
            </div>
        )
    }
}
GfsInputeDescriber.propTypes={
    labelKey:PropTypes.string,
    valueKey:PropTypes.string,
    value:PropTypes.oneOf([
        PropTypes.string,
        PropTypes.number
    ]),
    onChange:PropTypes.func
}
GfsInputeDescriber.defaultProps={
    onChange:()=>{}
}
export default GfsInputeDescriber;
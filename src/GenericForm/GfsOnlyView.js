var React = require('react');

export default class GfsOnlyView extends React.Component {
  render() {
    return (
      <div className="only-view">
          <p  
            className="label">{this.props.label}</p>
          {!this.props.component &&
            <p 
               className="value">{this.props.valueString||'-------'}</p>
          }{!!this.props.component && this.props.component}
      </div>
    )
  }
}
//style={{ fontWeight: '300', fontSize: '17px', color: '#8A9499', marginBottom: '3px' }}
//style={{ fontSize: '17px', paddingLeft: '10px', color: 'black', fontWeight: '600', color: '#3D474D' }}
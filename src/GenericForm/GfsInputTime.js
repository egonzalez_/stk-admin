/**
 * Created by estratek on 15/12/17.
 */
import GfsValidator, { getArrayErrors } from './GfsValidator'
import PropTypes from 'prop-types';
import GfsOnlyView from './GfsOnlyView';
import GfsDescriptionPopOver from './GfsDescriptionPopOver'
var React = require('react');
const Datetime = require('react-datetime');




class GfsInputTime extends React.Component {
    state = {
        popoverOpen: false,
    }
    componentWillUpdate(nextProps) {
        if (nextProps.value !== this.props.value && !!this.props.onChangeError) {
            let newErrorState = getArrayErrors(nextProps.validation, nextProps.value).length > 0;
            this.props.onChangeError(newErrorState);
        }
    }
    componentDidMount() {
        this.props.onChangeError(this.hasError());
    }
    handleChange = (event) => {
        if (typeof event.format == 'function') {
            this.props.onChange(event.format('HH:mm'));
        }
    }
    hasError = () => (
        !this.props.validation && getArrayErrors(this.props.validation, this.props.value).length > 0
    )

    render() {
        if (this.props.readOnly) {
            return (<GfsOnlyView label={this.props.label} valueString={this.props.value} />)
        }
        return (
            <div className="mb-2 gfs-input gfs-input-text">
                {this.props.label && <label className="form-control-label mb-1 "> <strong> {this.props.label}</strong>
                    {this.props.description && <GfsDescriptionPopOver description={this.props.description} />}
                </label>}
                <div className={"form-group input-group input-group-dropdown mb-0" + ((this.hasError()) ? 'has-error' : '')}>
                    {!!this.props.describerComponent && this.props.describerComponent}
                    <Datetime
                        onChange={this.handleChange}
                        value={this.props.value}
                        dateFormat={false}
                        timeFormat={"HH:mm"}
                        disabled={this.props.disabled}
                        placeholder={this.props.placeHolder || ''} />
                </div>
                <GfsValidator validation={this.props.validation}
                    value={this.props.value} />
            </div>
        );
    }
}




GfsInputTime.propTypes = {
    disabled: PropTypes.bool,
    type: PropTypes.string,
    validation: PropTypes.object,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    onChangeError: PropTypes.func,
    description: PropTypes.string,
    placeHolder: PropTypes.string,
    hasError: PropTypes.bool,
    label: PropTypes.string,
    readOnly: PropTypes.bool
};
GfsInputTime.defaultProps = {
    disabled: false,
    type: 'text',
    validation: {},
    value: '',
    onChange: () => { },
    onChangeError: () => { },
    description: "",
    placeHolder: "",
    hasError: false,
    label: "",
    readOnly: false
};

export default GfsInputTime;
/**
 * Created by estratek on 1/12/17.
 */
let React = require('react');
import GfsInput from './GfsInput';
import GfsInputTime from './GfsInputTime';
import GfsInputTextArea from './GfsInputTextArea';
import GfsInputFileImage from './GfsInputFileImage';
import GfsHyperLink from './GfsHyperLink';
import DynamicSelect from './DynamicSelect';
import GfsInputDatePicker from './GfsInputDatePicker';
import GenericCrud from './GenericCrud/GenericCrud';
import PropTypes from 'prop-types';
import moment from 'moment';
import GfsInputDescriber from './GfsInputDescriber';
import { getArrayErrors } from './GfsValidator';
import axios from 'axios'

class GenericForm extends React.Component {

    componentDidMount = () => {
        this.sendOnChange(this.props.structure);
    }

    transformStructure(structure, readOnly, _currentvalues) {
        let auxStructure = JSON.parse(JSON.stringify(structure))
        let newStructure = this.passArrayToSubtype(auxStructure);
        newStructure = this.setCurrentPropsValues(newStructure, _currentvalues);
        newStructure = this.deleteHiddenInputs(newStructure);
        newStructure = this.addClassColToFields(newStructure);
        if (!!readOnly == true) {
            newStructure = this.addOnlyViewProp(newStructure);
        }
        newStructure = this.groupInputs(newStructure);
        return newStructure;
    }
    setCurrentPropsValues = (_structures = [], _values = []) => {
        return _structures.map(_x => {
            if (typeof _values[_x.real_name] != 'undefined') {
                _x.value = _values[_x.real_name]
            }
            return _x;
        })
    }

    groupInputs = (_structureArray) => {
        let auxGroups = this.getAllTheDividers(_structureArray);
        let group = auxGroups
            .map(group => ({
                nombre: group, inputs: _structureArray
                    .filter(_field => _field.formGroup === group)
            }))
        group.unshift({ nombre: 'default', inputs: _structureArray.filter(_field => !_field.formGroup) })
        return group;
    }
    addOnlyViewProp = (_structureArray) => {
        return _structureArray.map((_val) => { _val.readOnly = true; return _val; })
    }

    deleteHiddenInputs = (structureWithHiddens) => {
        return structureWithHiddens.filter(value => !value.hidden)
    }
    //for obj that are in [] transform that array in only obj.
    passArrayToSubtype(_structureObj) {
        return Object.keys(_structureObj).map((_fieldKey) => {
            if (_structureObj[_fieldKey] instanceof Array) {
                _structureObj[_fieldKey][0]
            } else {
                _structureObj[_fieldKey]
            }
            //add the key to the object as a "fieldkey"
            _structureObj[_fieldKey].real_name = _fieldKey;
            return _structureObj[_fieldKey];
        })
    }
    addClassColToFields = (_structureArray) => {
        return _structureArray.map((_field) => {
            if (_field.subtype == 'textarea' || _field.subtype == 'file' || _field.subtype == 'dynamictable' || _field.subtype === 'range') {
                _field.classCss = "col-md-12 " + (_field.classCss || '') ;
            } else {
                _field.classCss = "col-md-6 " + (_field.classCss || '');
            }
            return _field;
        });
    }
    getDescriberComponent = (input, structureObj) => {
        let _structureObj = JSON.parse(JSON.stringify(structureObj))
        if (!input || !_structureObj) return false;
        let keyDescriber = Object.keys(_structureObj).find(_key => !!_structureObj[_key].describer && _structureObj[_key].describer == input.real_name);

        let describer = _structureObj[keyDescriber];
        let obj = { ..._structureObj[keyDescriber], real_name: keyDescriber };
        if (!describer) return false;
        let props = this.props.value && this.props.value.form || {};
        return <GfsInputDescriber  {...obj}
            onChange={(e) => this.handleChange(keyDescriber, e)}
            host={obj.host || this.props.host}
            ref={null}
            value={props[keyDescriber]}
            model={obj.ref || obj.model}
            labelKey={obj.labelkey}
            valueKey={obj.valuekey}
        />;
    }


    getInput(_input) {
        let input = JSON.stringify(_input[0]);
        if (_input instanceof Array) {
            input = JSON.parse(JSON.stringify(_input[0]));
            input.real_name = _input.real_name;
        } else {
            input = JSON.parse(JSON.stringify(_input));
        }
        input.validation = JSON.parse(JSON.stringify(input));
        input.onChange = (e) => this.handleChange(input.real_name, e)
        input.host = input.host || this.props.host
        input.model = input.ref || input.model;
        input.describerComponent = this.getDescriberComponent(input, this.props.structure);

        if (input.type == 'number') {
            return <GfsInput {...input} />
        }
        if (input.type == 'date') {            
            return <GfsInputDatePicker {...input} />
        }

        if (input.type == 'time') {
            return <GfsInputTime {...input} />
        }
        if (input.type == 'boolean') {
            return <GfsInput {...input} type="checkbox" />
        }
        if (((input.type == 'array' && input.subtype == 'ref') || input.subtype == 'ref')) {
            let aux = { ...input };
            delete aux.ref
            if (input.options) {
                return <DynamicSelect {...aux}
                    type={"ref"}
                    labelKey={input.labelkey}
                    options={input.options}
                    valueKey={input.valuekey}
                    axiosInstance={this.props.axiosInstance}
                    />;
            }
            return <DynamicSelect {...aux}
                type={"ref"}
                labelKey={input.labelkey}
                valueKey={input.valuekey}
                axiosInstance={this.props.axiosInstance}
                />;
        }
        if (!!input.ref && input.subtype == 'dynamictable') {
            let aux = { ...input };
            delete aux.ref;
            return <GenericCrud {...aux}
                title={input.label || input.real_name}
                hideVia={input.hideVia || true}
                saveRelationsAfterSave={true}
                populateInFrontEnd={true}
                viaValue={this.props.editingId}
                viaRelation={input.via} />
        }
        if (input.type == 'text' || input.type == 'string') {
            if (input.subtype == 'textarea') {
                return <GfsInputTextArea {...input} />
            }
            if (input.subtype == 'file') {
                return <GfsInputFileImage {...input} disabledButtonWhenUploading={this.props.disabledButtonWhenUploading}/>
            }
            if (input.subtype == 'link') {
                return <GfsHyperLink {...input}/>
            }
            if (input.subtype == "password" || input.subtype == "email") {
                return <GfsInput {...input} type={input.subtype} />
            }
            return <GfsInput {...input} type={"text"} />
        }
    }

    // this is mine
    getAllTheDividers = (_structure) => [... new Set(_structure.filter(el => el.formGroup).map(el => el.formGroup))];
    // this is mine

    handleChange = (real_name, newVal) => {
        let _newArray = JSON.parse(JSON.stringify(this.props.structure));
        !!_newArray[real_name] && (_newArray[real_name].value = newVal);

        //RANGE DATES TRANSFORM STRUCTURE
        if (_newArray[real_name].rangeDate) { 
            _newArray=this.setConfigDateRange(real_name, newVal,_newArray);            
        };
        this.sendOnChange(_newArray)
    }

    setConfigDateRange = (real_name,newVal,_structure) => {
        let structure = JSON.parse(JSON.stringify(_structure));        
        let updatedDate = structure[real_name];
        /* FIND START DATE */
        let firstDateKey=Object.keys(_structure).find(x=>{
            return (_structure[x].rangeDate  && _structure[x].rangeDate.id==updatedDate.rangeDate.id && _structure[x].rangeDate.pos=='start')
        });

        /* FIND END DATE */
        let lastDateKey=Object.keys(_structure).find(x=>{
            return (_structure[x].rangeDate  && _structure[x].rangeDate.id==updatedDate.rangeDate.id && _structure[x].rangeDate.pos=='end')
        });
        if (structure[firstDateKey]){
            structure[firstDateKey]={
                ...structure[firstDateKey],
                selectsStart:true,
                startDate:(structure[firstDateKey].value) || null,
                endDate:(structure[lastDateKey].value) || null,
            }
        }
        if (structure[lastDateKey]){
            structure[lastDateKey]={
                ...structure[lastDateKey],
                selectsEnd:true,
                startDate:(structure[firstDateKey].value) || null,
                endDate:(structure[lastDateKey].value) || null,             
            }
        }
        return structure;

    }


    sendOnChange = (newArray) => {
        let newStructureParsed = this.prepareDataToSend(newArray);
        let currentPropValue = newStructureParsed || {};
        let arrayErrors = this.checkIfFormHasError(this.props.structure, currentPropValue);
        //avoid to redraw if there where no error or new changes

        this.props.onChange({
            form: newStructureParsed,
            hasError: Object.keys(arrayErrors).length > 0,
            arrayErrors: arrayErrors,
            structure:newArray
        });
    }

    checkIfFormHasError(_structure, _currentValues) {
        let auxStructure = JSON.parse(JSON.stringify(_structure))
        let newStructure = this.passArrayToSubtype(auxStructure);
        newStructure = this.setCurrentPropsValues(newStructure, _currentValues);
        let arrayErrors = {};
        newStructure.map(_input => {
            let auxVal = getArrayErrors(_input, _input.value);
            if (auxVal.length > 0) {
                arrayErrors[_input.real_name] = auxVal
            }
        })
        return arrayErrors;
    }
    prepareDataToSend(data) {
        let newArray = JSON.parse(JSON.stringify(this.props.value && this.props.value.form || {}));
        for (let value in data) {
            if (typeof (data[value].value) !== 'undefined') {
                newArray[value] = data[value].value;
            }
        }
        return newArray;
    }

    render() {
        let currentPropValue = this.props.value && this.props.value.form || {};
        let transformedStructure = this.transformStructure(this.props.structure, this.props.readOnly, currentPropValue)        
        return (
            <div id="_stk_gforms" className="row" className={`stk-gforms generic-form ${this.props.cssClass || ''}`}>
                {
                    transformedStructure && transformedStructure.map((_group, _key) => (
                        <React.Fragment key={_key}>
                            {(!!_group.inputs.length > 0) && <div key={`cf_form${_key}`} className='container-fluid'>
                                <div className="row">
                                    <div className="col-md-12">
                                        {(_group.nombre === 'default') ? '' : (
                                            <div className="row separator-group">
                                                <div className="col-md-12 form-section-Title">
                                                    <h5><span>{_group.nombre}</span></h5>
                                                </div>
                                                {_group.descripcion &&
                                                    < div className="col-md-12 form-section-description">
                                                        <h5><span>{_group.descripcion}</span></h5>
                                                    </div>}
                                            </div>
                                        )}
                                    </div>
                                    {_group.inputs.map((_field, _key) =>
                                        (!!!_field.describer) ? (
                                            <div key={`${_group.name}_child_${_key}`} className={_field.classCss}>
                                                {this.getInput(_field)}
                                            </div>
                                        ) : null
                                    )}
                                </div>
                            </div>
                            }
                        </React.Fragment>
                    ))
                }
            </div>
        );
    }
}

GenericForm.propTypes = {
    host: PropTypes.string,
    transformStructure: PropTypes.func,
    onChange: PropTypes.func.isRequired,
    readOnly: PropTypes.bool,
    value: PropTypes.object,
    axiosInstance: PropTypes.instanceOf(axios)
};
GenericForm.defaultProps = {
    readOnly: false,
    onChange: () => { },
    transformStructure: (_structure) => _structure
};

export default GenericForm
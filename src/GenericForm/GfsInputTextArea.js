/**
 * Created by estratek on 15/12/17.
 */
var React = require('react');
import GfsValidator ,{getArrayErrors} from './GfsValidator';
import PropTypes from 'prop-types';
import GfsOnlyView from './GfsOnlyView';

class GfsInputTextArea extends React.Component {
    componentWillUpdate(nextProps){
        if (nextProps.value!==this.props.value){
            let newErrorState=getArrayErrors(nextProps.validation,nextProps.value).length>0;
            this.props.onChangeError(newErrorState);
        }
    }
    componentDidMount(){
        this.props.onChangeError(this.hasError());
    }
    hasError(){
        return getArrayErrors(this.props.validation,this.props.value).length>0;
    }

    handleChange(event){
        if (!this.props.onChange){return false};
        this.props.onChange(event.target.value);
    }
    render () {
        if (this.props.readOnly){
            return (<GfsOnlyView label={this.props.label} valueString={this.props.value}/>)
        }
        return (
            <div className={"form-group "+((this.hasError())?'has-error':'')}>
                {this.props.label && <strong className="form-control-label">{this.props.label}</strong>}
                <textarea
                    className={"form-control "+((this.hasError())?'form-control-danger':'')}
                          rows="3"
                          disabled={this.props.disabled}
                          value={this.props.value}
                          onChange={this.handleChange.bind(this)}/>
                <GfsValidator validation={this.props.validation}
                              value={this.props.value}/>
                {this.props.description && <small className="form-text text-muted">{this.props.description}</small>}
            </div>
        );
    }
}

GfsInputTextArea.propTypes = {
    disabled: PropTypes.bool,
    type: PropTypes.string,
    validation: PropTypes.object,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    onChangeError: PropTypes.func,
    description: PropTypes.string,
    placeHolder: PropTypes.string,
    hasError: PropTypes.bool,
    label: PropTypes.string,
};

GfsInputTextArea.defaultProps = {
    disabled: false,
    type: 'text',
    validation: {},
    value: '',
    onChange: ()=>{},
    onChangeError: ()=>{},
    description: "",
    placeHolder: "",
    hasError: false,
    label: "",
};

export default  GfsInputTextArea;
/**
 * Created by estratek on 15/12/17.
 */
var React = require('react');
var Popover = require('reactstrap').Popover;
var PopoverHeader = require('reactstrap').PopoverHeader;
var PopoverBody = require('reactstrap').PopoverBody;


export default class GfsDescriptionPopOver extends React.Component {
    constructor(props) {
        super(props);
        this.myRef = React.createRef();
        this.setWrapperRef = this.setWrapperRef.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
    }

    state = {
        popoverOpen: false
    }
    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }
    setWrapperRef(node) {
        this.wrapperRef = node;
    }
    /**
   * Alert if clicked on outside of element
   */
    handleClickOutside(event) {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {            
            this.setState({
                popoverOpen:false
            })
        }
    }

    toggle = () => {
        this.setState({
            popoverOpen: !this.state.popoverOpen
        });
    }

    render() {
        return (
            <span className='gfs-input-description' ref={this.setWrapperRef} >
                <button className="btn btn-sm gfs-btn-description" onClick={this.toggle} ref={this.myRef}>
                    <i className="fa fa-info" ></i>
                </button>
                <Popover placement="bottom"
                    isOpen={this.state.popoverOpen}
                    target={this.myRef.current}
                    autohide
                    className='stk-popover'
                    toggle={this.toggle} >
                    <PopoverBody>{this.props.description && this.props.description}</PopoverBody>
                </Popover>
            </span>
        );
    }
}

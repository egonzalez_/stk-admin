import React, { Component } from 'react'
import GfsValidator, { getArrayErrors } from './GfsValidator'
import PropTypes from 'prop-types';
import GfsOnlyView from './GfsOnlyView';
import GfsDescriptionPopOver from './GfsDescriptionPopOver'

export default class GfsHyperLink extends Component {
  state = {
    popoverOpen:false,
  }
  componentWillUpdate(nextProps) {
      if (nextProps.value !== this.props.value && !!this.props.onChangeError) {
          let newErrorState = getArrayErrors(nextProps.validation, nextProps.value).length > 0;
          this.props.onChangeError(newErrorState);
      }
  }
  componentDidMount() {
      this.props.onChangeError(this.hasError());
  }
  handleChange = (event) => {
      let newVal = (this.props.type == 'checkbox') ? event.target.checked : event.target.value;
      this.props.onChange(newVal);
  }
  hasError = () => (
      !this.props.validation && getArrayErrors(this.props.validation, this.props.value).length > 0
  )

  render() {
    return (
        <div className="only-view">
          <p className="label">
            {this.props.label}
            {this.props.description && <GfsDescriptionPopOver description={this.props.description} />}
          </p>
          <a
            className={"value"}
            style={{ textDecoration: "underline" }}
            href={this.props.value}
            target="_blank"
            // disabled={this.props.disabled}
          >
            {this.props.placeholder}
            <i style={{color: '#39AFBC', fontSize: '10px', paddingLeft: '5px'}} className="fa fa-external-link"/>
          </a>
        </div>
    );
  }
}


GfsHyperLink.defaultProps = {
  disabled: false,
  type: 'text',
  validation: {},
  value: '',
  onChange: () => { },
  onChangeError: () => { },
  description: "",
  placeHolder: "",
  hasError: false,
  label: "",
  readOnly: false
};
var React = require('react');
import PropTypes from 'prop-types';

class MockCrudHeaderRow extends React.Component {

    getFakeArray = (_iterations) => {
        let newArray = [];
        for (let index = 0; index < _iterations; index++) {
            newArray.push(index);
        }
        return newArray;
    }


    render() {
        return (
            <tr >
                {this.getFakeArray(this.props.numCols).map(_key =>
                    <th key={_key}> <div className="progress"></div>    </th>
                )}
            </tr>
        )
    }
}
MockCrudHeaderRow.propTypes = {
    numCols: PropTypes.number
}
MockCrudHeaderRow.defaultProps = {
    numCols: 3
}

class MockCrudBodyRow extends React.Component {

    getFakeArray = (_iterations) => {
        let newArray = [];
        for (let index = 0; index < _iterations; index++) {
            newArray.push(index);
        }
        return newArray;
    }


    render() {
        return (
            <React.Fragment>
                {this.getFakeArray(this.props.numCols).map((_permiso, _key) =>
                    <tr key={_key}>
                        {this.getFakeArray(this.props.numRows).map(_key =>
                            <td key={_key}> <div className="progress"></div>    </td>
                        )}
                    </tr>
                )}
            </React.Fragment>
        )
    }
}
MockCrudBodyRow.propTypes = {
    numCols: PropTypes.number,
    numRows: PropTypes.number,
}
MockCrudBodyRow.defaultProps = {
    numCols: 3,
    numRows: 3,
}

export default class MockCrudTable extends React.Component {
  render() {
    return (
      <table className="table table-hover">
          <thead> <MockCrudHeaderRow {...this.props}/>  </thead>
          <tbody> <MockCrudBodyRow {...this.props}/></tbody>
      </table>
    )
  }
}


export { MockCrudBodyRow, MockCrudHeaderRow };

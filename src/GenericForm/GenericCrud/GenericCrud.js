/**
 * Created by estratek on 15/12/17.
 */
var React = require('react');
var axios = require('axios');
import DynamicModal from './../DynamicModal';
import DynamicSelect from './../DynamicSelect';
import GenericCrudRowSearch from './GenericCrudRowSearch';
import DynamicInputTableConfigStructure from './../DynamicInputTableConfigStructure';
import { getUrlApi, getPrimaryKeyOfStructure } from './../utils';
import ModalError from './../ModalError';
import PropTypes from 'prop-types';
import { BarLoader } from 'react-spinners';
import MockCrudTable, {
  MockCrudBodyRow,
  MockCrudHeaderRow
} from './mockCrud/MockCrudTable';
var moment = require('moment');
import { UncontrolledTooltip } from 'reactstrap';

class GenericCrud extends React.Component {
  state = {
    structure: [],
    modalStructure: null,
    step: -1,
    limitItems: this.props.limitItems,
    hasMoreDataNext: true,
    hasMoreDataBack: false,
    loadingData: false,
    loadingStructure: false,
    showModalConfig: false,
    idEditingRow: '',
    showModalAdd: false,
    sortBy: {
      key: '',
      type: ''
    },
    structureSearch: {},
    showSearch: false,
    showModalAddDetail: false,
    populateStackRelations: {},
    totalCountRows: null,
    tooltipOpenAdd: false
  };

  componentDidMount() {
    this.getStructure();
  }

  getUrlAPI(_tipo) {
    return getUrlApi(_tipo, this.props);
  }

  componentDidUpdate(prevProps, prevState, prevContext) {
    if (prevProps.refreshData != this.props.refreshData) {
      this.getData();
    }
    if (
      prevProps.host != this.props.host ||
      prevProps.microservice != this.props.microservice ||
      prevProps.model != this.props.model
    ) {
      this.setState(
        {
          structure: [],
          modalStructure: null,
          step: -1,
          limitItems: this.prevProps.limitItems,
          hasMoreDataNext: true,
          hasMoreDataBack: false,
          loadingData: false,
          loadingStructure: false,
          showModalConfig: false,
          idEditingRow: '',
          showModalAdd: false,
          sortBy: {
            key: '',
            type: ''
          },
          structureSearch: {},
          showSearch: false
        },
        () => this.getStructure
      );
    }
  }

  getLimitItems() {
    return this.state.limitItems;
  }
  handleChangeLimitItems = ({ target }) => {
    this.setState({ limitItems: parseInt(target.value) }, () => this.getData());
  };
  steperData(_direction) {
    if (_direction > 0) {
      this.setState({ step: this.state.step + 1 }, () => this.getData());
    } else if (this.state.step > 0) {
      this.setState({ step: this.state.step - 1 }, () => this.getData());
    }
  }
  getData(dateSelectors) {
    let params = this.prepareParamsGetData(dateSelectors);
    this.setState({ loadingData: true });
    this.countRows(dateSelectors);
    if (this.props.axiosInstance) {
      this.props.axiosInstance
        .get(this.getUrlAPI(), { params: params })
        .then(res => {
          this.populateInFrontEnd &&
            this.populateRelations(this.state.structure, res.data);
          this.setState(
            {
              loadingData: false,
              hasMoreDataNext: !(res.data.length < this.state.limitItems)
            },
            () => this.onChange(res.data)
          );
        })
        .catch(err => {
          this.setState({ loadingData: false });
        });
    } else {
      axios
        .get(this.getUrlAPI(), { params: params })
        .then(res => {
          this.populateInFrontEnd &&
            this.populateRelations(this.state.structure, res.data);
          this.setState(
            {
              loadingData: false,
              hasMoreDataNext: !(res.data.length < this.state.limitItems)
            },
            () => this.onChange(res.data)
          );
        })
        .catch(err => {
          this.setState({ loadingData: false });
        });
    }
  }
  prepareParamsGetData = dateSelectors => {
    let params = {
      limit: this.getLimitItems(),
      skip: this.getLimitItems() * this.state.step
    };
    if (!!this.state.sortBy.key && !!this.state.sortBy.type) {
      let sortObj = {};
      if (this.props.isSails) {
        params['sort'] = `${
          this.state.sortBy.key
        } ${this.state.sortBy.type.toUpperCase()}`;
      } else {
        sortObj[this.state.sortBy.key] = this.state.sortBy.type;
        params['sort'] = sortObj;
      }
    }
    if (!!this.props.viaRelation && !!this.props.viaValue) {
      params[this.props.viaRelation] = this.props.viaValue;
    }
    if (!!this.state.showSearch) {
      if (this.props.isSails) {
        params = {
          ...params,
          where: {
            ...params.where,
            ...this.getClearStructureVals(
              this.state.structureSearch,
              dateSelectors
            )
          }
        };
      } else {
        params = {
          ...params,
          ...this.getClearStructureVals(this.state.structureSearch, dateSelectors)
        };
      }
    }
    return { ...this.props.paramsApi, ...params };
  };

  countRows = (dateSelectors = undefined) => {
    //si fallo la primera vez en consegeuir el total, se asume que esta restApi no tiene la propiedad para contar el total
    if (this.state.step > 0 && !this.state.totalCountRows) {
      return false;
    }

    let params = this.prepareParamsGetData(dateSelectors);
    this.setState({ totalCountRows: null });
    // If someone use sails
    if (this.props.isSails) {
      if (this.props.axiosInstance) {
        return this.sailsCounter(params, true);
      }
    }
    // when you are not so cool
    params._countAll = true;
    axios.get(this.getUrlAPI(), { params: params }).then(({ data }) => {
      this.setState({ totalCountRows: data || null });
    });
  };

  sailsCounter = (params, axiosInstance = false) => {
    let instanceAxios = axiosInstance ? this.props.axiosInstance : axios;
    if(this.props.microservice) {
      instanceAxios
        .get(
          this.props.host +
            '/' +
            this.props.microservice +
            '/count/' +
            this.props.model,
          { params: params }
        )
        .then(({ data }) => {
          this.setState({ totalCountRows: data.count || null });
        });
    } else {
      instanceAxios
        .get(
          this.props.host +
            '/count/' +
            this.props.model,
          { params: params }
        )
        .then(({ data }) => {
          this.setState({ totalCountRows: data.count || null });
        });
    }
  };

  getPageStatus = () => {
    return (
      this.state.step +
      1 +
      '/' +
      Math.ceil(this.state.totalCountRows / this.state.limitItems)
    );
  };

  getClearStructureVals = (_structureSearch, dateSelectors) => {
    let newArray = {};
    this.prepareStructureToPrint(_structureSearch)
      .filter(_field => !!_field.value)
      .map(_value => {
        if (!!_value.ref || !!_value.model) {
          newArray[_value.real_name] = _value.value;
        } else if (_value.type === 'date') {
          if (
            _value.value[_value.real_name].start !== null ||
            _value.value[_value.real_name].end !== null
          ) {
            // get selectos for keyValue of date inputs
            let aux = dateSelectors && dateSelectors[_value.real_name];
            // Set filters
            let filter = {
              '>=': aux ? aux.start : '',
              '<=': aux ? aux.end : ''
            };
            // delete null propertie for filter
            let _filter = Object.keys(filter).reduce((obj, key) => {
              if (filter[key]) {
                obj[key] = filter[key];
              }
              return obj;
            }, {});
            // if object is empty dont set filter
            if (this.props.isSails) {
              newArray[_value.real_name] = _filter;
            } else {
              newArray[_value.real_name] = _filter;
            }
          }
        } else {
          newArray[_value.real_name] = {
            contains: _value.value
          };
        }
        return _value;
      });
    return newArray;
  };
  getStructure() {
    this.setState({ loadingStructure: true });
    axios
      .get(this.getUrlAPI('structure'))
      .then(({ data }) => {
        //let structureFetched = this.prepareStructureToPrint(JSON.parse(data.structure));
        let structure = this.props.transformStructure(
          JSON.parse(data.structure)
        );
        this.setState(
          {
            loadingStructure: false,
            structure: structure,
            structureSearch: structure
          },
          () => this.steperData(1)
        );
      })
      .catch(err => {
        this.setState({ loadingStructure: false });
      });
  }
  prepareStructureToPrint = _structureObj => {
    let structureObj = JSON.parse(JSON.stringify(_structureObj));
    let val = Object.keys(structureObj)
      .map(_fieldKey => {
        if (structureObj[_fieldKey] instanceof Array) {
          structureObj[_fieldKey] = structureObj[_fieldKey][0];
        }
        //add the key to the object as a "fieldkey"
        structureObj[_fieldKey].real_name = _fieldKey;
        return structureObj[_fieldKey];
      })
      .filter(_field => _field.subtype != 'dynamictable')
      .filter(_x => _x.real_name != '_selfTablePermissions')
      .filter(_field => (_field.hiddenTable == false) ? (!_field.hiddenTable) : (!_field.hidden))
    if (!!this.props.hideVia && !!this.props.viaRelation) {
      val = val.filter(_field => _field.real_name != this.props.viaRelation);
    }
  
    // retorna la estructura que contendra el dinamic table
    return val;
  };
  openAddModal = () => {
    this.setState({ showModalAdd: true });
  };
  openEditModal(_obj) {
    this.setState({ showModalEdit: true, idEditingRow: _obj });
  }
  openTableEdit() {
    this.setState({ showModalConfig: true });
  }
  onChangeStructure = _newStructure => {
    this.setState({ structure: _newStructure, showModalConfig: false });
  };
  onUpdate(obj, e) {
    this.getData();
  }
  onChangeStructureSearch = (_newStructure, dateSelectors = {}) => {
    this.setState(
      { structureSearch: _newStructure },
      this.getData(dateSelectors)
    );
  };
  deleteItem(e, keyPrimary) {
    if (!confirm('Desea eliminar este elemento?')) {
      return false;
    }
    if (!!this.props.fakeSave) {
      return this.onDelete(e);
    }
    this.setState({ loadingData: true });
    axios
      .delete(this.getUrlAPI() + '/' + e[keyPrimary])
      .then(res => {
        this.setState({ loadingData: false });
        this.onDelete(e);
      })
      .catch(err => {
        this.setState({ loadingData: false, error: err });
        console.error('error deleting GenericCrud.deleteItem()', err);
      });
  }
  onDelete(e) {
    this.getData();
  }
  onSave(e) {
    this.setState({ showModalAdd: false, showModalEdit: false }, this.getData);
  }
  onChange(_newVal) {
    if (!!this.props.onChange) {
      this.props.onChange(_newVal);
    }
  }
  getValueToShow(_objRef, _objStructureRef) {
    //console.log('_objStructureRef',_objStructureRef,_objRef);
    if (typeof _objRef == 'undefined') {
      return '';
    }

    if (_objRef == null) {
      return '';
    }

    if (_objStructureRef.subtype == 'file') {
      return (
        <img
          src={_objRef}
          alt={_objStructureRef.real_name}
          style={{ maxWidth: '2em', maxHeight: '2em' }}
        />
      );
    }
    if (_objStructureRef.type == 'boolean') {
      return <input type="checkbox" disabled="true" defaultChecked={_objRef} />;
    }
    if (_objStructureRef.type == 'date') {
      return (
        _objRef.length !== 0 ? <div className="dateInput">
          <i className="fa fa-calendar" aria-hidden="true" />{' '}
          {moment(_objRef)
            .locale('es')
            .utc()
            .format(this.props.formatDates) || ''}
        </div>:<div></div>
      );
    }
    if (typeof _objRef == 'object' && !!_objRef) {
      return _objRef[_objStructureRef.labelkey];
    }
    //retorna el valor a imprimir dentro del dinamic table
    return _objRef;
  }
  getPrimaryKey(structure) {
    if (!structure) return false;
    return getPrimaryKeyOfStructure(structure);
  }

  sortBy = _key => {
    let typeSort = 'asc';
    if (_key == this.state.sortBy.key) {
      switch (this.state.sortBy.type) {
        case '':
          typeSort = 'asc';
          break;
        case 'asc':
          typeSort = 'desc';
          break;
        case 'desc':
          typeSort = '';
          break;
        default:
          typeSort = '';
      }
    }
    this.setState(
      {
        sortBy: {
          key: _key,
          type: typeSort
        }
      },
      () => this.getData()
    );
  };

  getSortClass = _key => {
    if (this.state.sortBy.key == _key && !!this.state.sortBy.type) {
      return '-' + this.state.sortBy.type;
    }
    return '';
  };
  capitalizeFirstLetter = string =>
    string.charAt(0).toUpperCase() + string.slice(1);

  transformStructureEdit = _structure => {
    let newStructure = JSON.parse(JSON.stringify(_structure));
    if (newStructure[this.props.viaRelation] && this.props.viaValue) {
      newStructure[this.props.viaRelation].value = this.props.viaValue;
      newStructure[this.props.viaRelation].disabled = true;

      if (this.props.transformStructureToModalEdit) {
        return this.props.transformStructureToModalEdit(newStructure);
      }
    }

    if (this.props.transformStructureToModalEdit) {
      return this.props.transformStructureToModalEdit(newStructure);
    }
    return newStructure;
  };
  transformStructureAdd = _structure => {
    let newStructure = JSON.parse(JSON.stringify(_structure));
    if (newStructure[this.props.viaRelation]) {
      newStructure[this.props.viaRelation].hidden = true;
      newStructure[this.props.viaRelation].required = false;
      if (this.props.viaValue) {
        newStructure[this.props.viaRelation].value = this.props.viaValue;
      }
      // check if the prop exist
      if (this.props.transformStructureToModalAdd) {
        let _str = this.props.transformStructureToModalAdd(newStructure);
        return _str;
      }
    }
    // check if the prop exist
    if (this.props.transformStructureToModalAdd) {
      let _str = this.props.transformStructureToModalAdd(newStructure);
      return _str;
    }
    return newStructure;
  };
  onCloseAddModal = _nose => {
    this.setState({ showModalAdd: false });
  };
  getTitle = model => {
    if (model.search('detalle') === 0) {
      return 'Nuevo Detalle ' + this.capitalizeFirstLetter(model.slice(7));
    } else if (model.search('tipo') === 0) {
      return 'Nuevo Tipo ' + this.capitalizeFirstLetter(model.slice(4));
    } else {
      if (this.props.title) {
        return 'Nuevo ' + this.capitalizeFirstLetter(this.props.title);
      }
      return this.capitalizeFirstLetter(model);
    }
  };
  populateRelations = async (_structure, _array) => {
    let newStructure = {};
    let structure = JSON.parse(JSON.stringify(_structure));
    Object.keys(_structure)
      .filter(x => {
        return (
          (Array.isArray(_structure[x]) &&
            !!_structure[x][0].ref &&
            !_structure[x][0].hidden) ||
          (!!_structure[x].ref && !_structure[x].hidden)
        );
      })
      .map(x => {
        newStructure[x] = Array.isArray(_structure[x])
          ? _structure[x][0]
          : _structure[x];
      });
    return await this.getPopulatesKeys(newStructure, _array);
  };
  getPopulatesKeys = async (_structureRelations, _array) => {
    let populateAjax = [];
    Object.keys(_structureRelations).map(key => {
      //FILTER IDS OF  FOREING KEYS
      let foreignsKeys = _array
        .filter(row => !Array.isArray(row[key]) && !!row[key])
        .map(row => row[key]);
      foreignsKeys = [...new Set(foreignsKeys)];
      //IF FOREIGNKEYS ARE NOT EMPTY PUSH ON POPULATE AJAX
      if (foreignsKeys.length) {
        populateAjax.push({
          structure: { ..._structureRelations[key], real_name: key },
          ids: foreignsKeys
        });
      }
    });
    let resultAjax = {};
    for (let value in populateAjax) {
      let auxStructure = populateAjax[value].structure;
      let params = {};
      params[auxStructure.valuekey] = { in: populateAjax[value].ids };
      let res = await axios.get(
        getUrlApi(null, {
          ...auxStructure,
          host: this.props.host,
          model: auxStructure.ref
        }),
        {
          params: params
        }
      );
      resultAjax[auxStructure.real_name] = res.data;
    }
    this.setState({ populateStackRelations: resultAjax });
  };
  transformPrintData = _arrayData => {
    return this.autoPopulateWithStack(_arrayData);
  };
  autoPopulateWithStack = _arrayData => {
    return _arrayData.map(x => {
      let aux = JSON.parse(JSON.stringify(x));
      Object.keys(this.state.populateStackRelations).map(y => {
        if (
          aux[y] &&
          Object.prototype.toString.call(aux[y]) != '[object Object]'
        ) {
          let res = this.state.populateStackRelations[y].find(
            z => z[this.state.structure[y].valuekey] == aux[y]
          );
          aux[y] = res;
        }
      });
      return aux;
    });
  };

  componentWillReceiveProps = nextProps => {
    if (JSON.stringify(this.props.value) != JSON.stringify(nextProps.value)) {
      if (this.props.populateInFrontEnd) {
        this.populateRelations(this.state.structure, nextProps.value || []);
      }
    }
    /* if(this.props.limitItems != nextProps.limitItems){
      this.setState(({
        limitItems: 10,
      }));
    }else{
      this.setState(({
        limitItems: this.props.limitItems,
      }));
    } */
  };
  hasMoreDataNext = () => {
    let totalRows = this.state.totalCountRows;
    if (totalRows) {
      return totalRows > (this.state.step + 1) * this.state.limitItems;
    }
    return this.state.hasMoreDataNext;
  };

  togglePP = () => {
    this.setState(prevState => ({ togglePP: !prevState.togglePP }));
  };
  toggleTTAdd = () => {
    this.setState(prevState => ({ tooltipOpenAdd: !prevState.tooltipOpenAdd }));
  };

  render() {
    return (
      <div className="gfs-crud">
        <div className="">
          <ModalError
            value={this.state.error}
            onAccept={e => {
              this.setState({ error: null });
            }}
          />
          {!this.props.hideTitle && (
            <div className="form-section-Title gfs-title-crud">
              <h5>
                <span>{this.capitalizeFirstLetter(this.props.title)}</span>
              </h5>
            </div>
          )}
          <div className="d-flex justify-content-end gfs-crud-section-options">
            <div className="btn-group btn-group-sm m-b-5">
              <button
                className="btn btn-outline-muted btn-sm gfs-crud-search"
                onClick={e =>
                  this.setState({ showSearch: !this.state.showSearch })
                }
              >
                <i className="fa fa-search" />
              </button>
              <button
                className="btn btn-outline-muted btn-sm gfs-crud-options"
                onClick={e => this.openTableEdit(e)}
              >
                <i className="fa fa-ellipsis-v" />
              </button>
              {!this.props.readOnly &&
                !this.props.hideAddButton &&
                !(
                  this.state.structure &&
                  this.state.structure._selfTablePermissions &&
                  this.state.structure._selfTablePermissions.hideAddButton
                ) && (
                  <button
                    className="btn btn-outline-default btn-sm gfs-crud-add"
                    onClick={this.openAddModal}
                  >
                    Agregar
                  </button>
                )}
            </div>
          </div>
          {!!this.props.description && (
            <div className="d-flex justify-content-between">
              <small>{this.props.description}</small>
            </div>
          )}
        </div>
        <DynamicInputTableConfigStructure
          isOpen={this.state.showModalConfig}
          structure={this.state.structure}
          onSave={this.onChangeStructure}
        />
        <DynamicModal
          isOpen={this.state.showModalAdd}
          model={this.props.model}
          onSave={this.onSave.bind(this)}
          saveRelationsAfterSave={this.props.saveRelationsAfterSave}
          onClose={this.onCloseAddModal}
          fakeSave={this.props.saveRelationsAfterSave || this.props.fakeSave}
          microservice={this.props.microservice}
          host={this.props.host}
          transformStructure={this.transformStructureAdd}
          title={this.getTitle(this.props.model)}
          axiosInstance={this.props.axiosInstance}
          // title={this.props.model.search('detalle') === 0 || this.props.model.search('tipo') === 0 ? 'Nuevo Detalle ' + this.props.title : this.capitalizeFirstLetter(this.props.model)}
        />
        {/*EDITING*/}
        <DynamicModal
          isOpen={this.state.showModalEdit}
          model={this.props.model}
          onUpdate={e => this.onUpdate(this.state.idEditingRow, e)}
          onDelete={e => this.onDelete(this.state.idEditingRow)}
          onClose={() => this.setState({ showModalEdit: false })}
          fakeSave={this.props.fakeSave}
          id={this.state.idEditingRow[this.getPrimaryKey(this.state.structure)]}
          microservice={this.props.microservice}
          transformStructure={this.transformStructureEdit}
          host={this.props.host}
          // title={'Editar ' + this.props.model.charAt(0).toUpperCase() + this.props.model.slice(1)}
          title={'Editar ' + this.capitalizeFirstLetter(this.props.title)}
        />
        <div className="">
          {(this.state.loadingData || this.state.loadingStructure) && (
            <div className="stk-Forms-loading">
              {' '}
              <BarLoader width={100} />{' '}
            </div>
          )}
          {!!this.state.structure &&
            !!this.props.value &&
            !this.state.loadingStructure && (
              <div style={{ overflowX: 'scroll' }}>
                <table className="table table-hover">
                  <thead>
                    <tr>
                    {!this.props.readOnly && !this.props.notActions && (
                        <th>Acciones</th>
                      )}
                      {this.prepareStructureToPrint(this.state.structure).map(
                        key => (
                          <th
                            key={key.real_name}
                            onClick={e => this.sortBy(key.real_name)}
                          >
                            {key.label || key.real_name}
                            <i
                              className={
                                'p-l-5 text-muted fa fa-sort' +
                                this.getSortClass(key.real_name)
                              }
                            />
                          </th>
                        )
                      )}
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.showSearch && (
                      <GenericCrudRowSearch
                        structureArray={this.state.structureSearch}
                        transformFunction={this.prepareStructureToPrint}
                        host={this.props.host}
                        onChange={this.onChangeStructureSearch}
                        axiosInstance={this.props.axiosInstance}
                      />
                    )}
                    {this.transformPrintData(this.props.value).map(
                      (row, keyRow) => (
                        <tr key={keyRow} className="text-center">
                          {!this.props.readOnly && !this.props.notActions && (
                            <React.Fragment>
                              <td>
                                <div id={'actions_btn' + keyRow}>
                                  {!this.props.hideEditButton &&
                                    !(
                                      this.state.structure &&
                                      this.state.structure
                                        ._selfTablePermissions &&
                                      this.state.structure._selfTablePermissions
                                        .hideUpdateButton
                                    ) && (
                                      <button
                                        style={{
                                          cursor:
                                            (row[
                                              this.props.inmutablePropChild
                                            ] &&
                                              row[this.props.inmutablePropChild]
                                                .length > 0) ||
                                            false
                                              ? 'not-allowed'
                                              : 'pointer'
                                        }}
                                        className="btn btn-default btn-square-icon m-r-5"
                                        id="action_btn_add"
                                        type="button"
                                        disabled={
                                          (row[this.props.inmutablePropChild] &&
                                            row[this.props.inmutablePropChild]
                                              .length > 0) ||
                                          false
                                        }
                                        onClick={e => this.openEditModal(row)}
                                      >
                                        <i className="fa fa-edit" />
                                      </button>
                                    )}
                                  {!this.props.hideDeleteButton &&
                                    !(
                                      this.state.structure &&
                                      this.state.structure
                                        ._selfTablePermissions &&
                                      this.state.structure._selfTablePermissions
                                        .hideDeleteButton
                                    ) && (
                                      <button
                                        style={{
                                          cursor:
                                            (row[
                                              this.props.inmutablePropChild
                                            ] &&
                                              row[this.props.inmutablePropChild]
                                                .length > 0) ||
                                            false
                                              ? 'not-allowed'
                                              : 'pointer'
                                        }}
                                        className="btn btn-danger btn-square-icon m-r-5"
                                        type="button"
                                        id={'action_btn_delete'}
                                        disabled={
                                          (row[this.props.inmutablePropChild] &&
                                            row[this.props.inmutablePropChild]
                                              .length > 0) ||
                                          false
                                        }
                                        onClick={e =>
                                          this.deleteItem(
                                            row,
                                            this.getPrimaryKey(
                                              this.state.structure
                                            )
                                          )
                                        }
                                      >
                                        <i className="fa fa-trash" />
                                      </button>
                                    )}
                                  {this.props.extraOptionRow.map(
                                    (_extraOption, _key) => (
                                      <React.Fragment key={_key}>
                                        {React.cloneElement(_extraOption, {
                                          onClick: e =>
                                            _extraOption.props.onClick(e, row),
                                          id: _key + 1 + '_btn',
                                          key: _key,
                                          style: {
                                            cursor:
                                              (row[
                                                this.props.inmutablePropChild
                                              ] &&
                                                row[
                                                  this.props.inmutablePropChild
                                                ].length > 0) ||
                                              false
                                                ? 'not-allowed'
                                                : 'pointer'
                                          },
                                          disabled:
                                            (row[
                                              this.props.inmutablePropChild
                                            ] &&
                                              row[this.props.inmutablePropChild]
                                                .length > 0) ||
                                            false
                                        })}
                                      </React.Fragment>
                                    )
                                  )}
                                  {this.props.extraOptionRowAsAFunction &&
                                    this.props
                                      .extraOptionRowAsAFunction(row)
                                      .map((_extraOption, _key) => (
                                        <React.Fragment>
                                          {React.cloneElement(_extraOption, {
                                            onClick: e =>
                                              _extraOption.props.onClick(
                                                e,
                                                row
                                              ),
                                            id: _key + 1 + '_btn',
                                            key: _key,
                                            style: {
                                              cursor:
                                                (row[
                                                  this.props.inmutablePropChild
                                                ] &&
                                                  row[
                                                    this.props
                                                      .inmutablePropChild
                                                  ].length > 0) ||
                                                false
                                                  ? 'not-allowed'
                                                  : 'pointer'
                                            },
                                            disabled:
                                              (row[
                                                this.props.inmutablePropChild
                                              ] &&
                                                row[
                                                  this.props.inmutablePropChild
                                                ].length > 0) ||
                                              false
                                          })}
                                        </React.Fragment>
                                      ))}
                                </div>
                                {((row[this.props.inmutablePropChild] &&
                                  row[this.props.inmutablePropChild].length >
                                    0) ||
                                  false) && (
                                  <UncontrolledTooltip
                                    placement="bottom"
                                    target={'actions_btn' + keyRow}
                                  >
                                    Esta acción esta deshabilitada, por que los
                                    datos estan ligados a otros.
                                  </UncontrolledTooltip>
                                )}
                              </td>
                            </React.Fragment>
                          )}
                          {this.prepareStructureToPrint(
                            this.state.structure
                          ).map((_structureField, _key) => (
                            <td key={_key}>
                              {this.getValueToShow(
                                row[_structureField.real_name],
                                _structureField
                              )}
                            </td>
                          ))}
                          
                        </tr>
                      )
                    )}
                  </tbody>
                </table>

                {!this.props.value.length && (
                  <h5 className="text-center text-muted">
                    No se encontraron registros
                  </h5>
                )}
              </div>
            )}
        </div>

        <div className="d-flex justify-content-end gfs-crud-paginator">
          <div className="btn-group btn-group-sm">
            <button
              className="btn btn-secondary gfs-crud-back"
              disabled={this.state.step <= 0}
              type="button"
              title="Atras"
              onClick={() => this.steperData(-1)}
            >
              <i className="fa fa-chevron-left" />
            </button>
            <button className="btn btn-secondary">
              {this.getPageStatus()}
            </button>
            {this.state.totalCountRows && (
              <button
                className="btn btn-secondary gfs-crud-next"
                type="button" 
                disabled={!this.hasMoreDataNext()}
                title="Siguiente"
                onClick={() => this.steperData(1)}
              >
                <i className="fa fa-chevron-right" />
              </button>
            )}
            <select
              name="select"
              onChange={this.handleChangeLimitItems}
              value={this.state.limitItems}
              className="form-control-sm gfs-crud-select"
              style={{ width: '7.5em' }}
            >
              {[5, 10, 25, 50, 100, 500, 1000].map((_value, _key) => (
                <option value={_value} key={_key}>
                  {_value} Items
                </option>
              ))}
            </select>
          </div>
        </div>
      </div>
    );
  }
}

GenericCrud.propTypes = {
  host: PropTypes.string.isRequired,
  model: PropTypes.string,
  microservice: PropTypes.string,
  viaRelation: PropTypes.string,
  saveRelationsAfterSave: PropTypes.bool,
  viaValue: PropTypes.string,
  onlyview: PropTypes.bool,
  hideVia: PropTypes.bool,
  hideAddButton: PropTypes.bool,
  fakeSave: PropTypes.bool,
  hideUpdateButton: PropTypes.bool,
  hideDeleteButton: PropTypes.bool,
  populateInFrontEnd: PropTypes.bool,
  paramsApi: PropTypes.object,
  title: PropTypes.string,
  extraOptionRow: PropTypes.array,
  transformStructure: PropTypes.func,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
  refreshData: PropTypes.bool,
  formatDates: PropTypes.string,
  extraOptionRowAsAFunction: PropTypes.func,
  limitItems: PropTypes.string,
};
GenericCrud.defaultProps = {
  formatDates: 'DD MMM YYYY',
  hideAddButton: false,
  hideUpdateButton: false,
  hideDeleteButton: false,
  fakeSave: false,
  saveRelationsAfterSave: false,
  populateInFrontEnd: false,
  hideTitle: false,
  title: '',
  extraOptionRow: [],
  paramsApi: {},
  refreshData: false, //si cambia el valor se ejecutara de nuevo getData()
  transformStructure: e => e,
  extraOptionRowAsAFunction: () => [],
  limitItems: '10',
};

export default GenericCrud;

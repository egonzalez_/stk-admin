
var React = require('react');
import PropTypes from 'prop-types';
import DynamicSelect from './../DynamicSelect';
import InputDatePicker from '../GfsInputDatePicker';
import DatePicker, { registerLocale } from 'react-datepicker';

class GenericCrudRowSearch extends React.Component {
    state = {}
    
    handleChange = (_key, _value) => {
        let structure = { ...this.props.structureArray };
        structure[_key].value = _value;
        this.props.onChange(structure);
    }

    handleChangeDateStart = (_key, _value) => {
        let structure = { ...this.props.structureArray };
        let _state = { ...this.state, [_key]: { ...this.state[_key], start: _value } }
        structure[_key].value = _state;
        this.setState((prevState) => ({ [_key] : { ...prevState[_key], start: _value } }))
        this.props.onChange(structure, _state)
    }

    handleChangeDateEnd = (_key, _value) => {
        let structure = { ...this.props.structureArray };
        let _state = { ...this.state, [_key]: { ...this.state[_key], end: _value } }
        structure[_key].value = _state;
        this.setState((prevState) => ({ [_key]: { ...prevState[_key], end: _value } }))
        this.props.onChange(structure, _state)
    }

    render() {
        if (!this.props.structureArray) { return (null) }
        return (
            <tr>
                <th></th>
                {this.props.transformFunction(this.props.structureArray).map((_structureField, _key) =>
                    {
                        return (<th key={_key}>
                        {
                            (_structureField.ref || _structureField.model) ?
                                <DynamicSelect
                                    value={_structureField.value}
                                    model={_structureField.model || _structureField.ref}
                                    microservice={_structureField.microservice}
                                    host={this.props.host}
                                    labelKey={_structureField.labelkey}
                                    valueKey={_structureField.valuekey}
                                    hideAddButton={true}
                                    hideUpdateButton={true}
                                    classWrapp={' '}
                                    onChange={(e) => this.handleChange(_structureField.real_name, e)}
                                    placeholder={_structureField.placeHolder || _structureField.label}
                                    axiosInstance={this.props.axiosInstance}
                                    />
                                : _structureField. type !== 'date' ? (<input type="text"
                                    className="form-control form-control-sm"
                                    value={_structureField.value || ''}
                                    onChange={(e) => this.handleChange(_structureField.real_name, e.target.value)}
                                    placeholder={_structureField.label || _structureField.real_name} />)
                                    : (<React.Fragment >
                                            <div style={{ display: 'flex' }}>
                                                <InputDatePicker
                                                    key={_structureField.real_name + '_start'}
                                                    placeHolder={'De:'}
                                                    startDate={(this.state[_structureField.real_name] && this.state[_structureField.real_name].start) || ''}
                                                    value={(this.state[_structureField.real_name] && this.state[_structureField.real_name].start) || ''}
                                                    onChange={(e) => this.handleChangeDateStart(_structureField.real_name, e)}
                                                    selectsStart
                                                />
                                                <InputDatePicker
                                                    startDate={(this.state[_structureField.real_name] && this.state[_structureField.real_name].start) || ''}
                                                    endDate={(this.state[_structureField.real_name] && this.state[_structureField.real_name].end) || ''}
                                                    value={(this.state[_structureField.real_name] && this.state[_structureField.real_name].end) || ''}
                                                    key={_structureField.real_name + '_end'}
                                                    placeHolder={'Hasta:'} value={(this.state[_structureField.real_name] && this.state[_structureField.real_name].end) || ''}
                                                    onChange={(e) => this.handleChangeDateEnd(_structureField.real_name, e)}
                                                    selectsEnd
                                                />
                                            </div>
                                        </React.Fragment>)
                        }
                    </th>)}
                )}
                
            </tr>
        );
    }
}

export default GenericCrudRowSearch;
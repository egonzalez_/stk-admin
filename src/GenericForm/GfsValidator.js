/**
 * Created by estratek on 15/12/17.
 */
var React = require('react');

export function getArrayErrors(_validations,_value=null){
        if (!_validations || typeof _value=='undefined'){return []}
        let propValue=_value;
        let newArray=[];

        Object.keys(_validations).map((value,index)=>{
            if (value=='minLength' && propValue.length<_validations[value]){
                newArray.push('Debe contener minimo '+_validations[value]+' caracteres');
            }
            if (value=='maxLength' && propValue.length>_validations[value]){

                newArray.push('debe contener maximo '+_validations[value]+' caracteres');
            }
            if (value=='max' && parseInt(propValue)>parseInt(_validations[value])){
                newArray.push('valor maximo '+_validations[value]);
            }
            if (value=='min' && parseInt(propValue)<parseInt(_validations[value])){
                newArray.push('valor minimo '+_validations[value]);
            }
            if ((value=='required' && (_validations[value]==true)  ) && (propValue=='' || typeof propValue =='undefined' || propValue ==null ) ){
                newArray.push('Campo Requerido');
            }
        });
        return newArray;
}

export default class GfsValidator extends React.Component{
    render(){
        return (
            <div>
                {getArrayErrors(this.props.validation,this.props.value).map((value,index)=>
                    <div className="form-control-feedback"
                         key={index}>
                        {value}
                    </div>
                )}
            </div>
        );
    }
}

/**
 * Created by estratek on 15/12/17.
 */
var React = require('react');
var axios = require('axios');
import GenericForm from './GenericForm';
import {getUrlApi} from './utils'
import {BarLoader} from 'react-spinners';
import PropTypes from 'prop-types';

class GenericFormApi extends React.Component {
    state={
        structure:{},
        loadingStructure:false,
        loadingData:false
    };
    omitArraysInStructure(structure){
        let newArray={};
        for (var value in structure){
            newArray[value]= (structure[value] instanceof Array) ?structure[value][0]:structure[value];
        }
        return newArray;
    }
    componentDidMount(){
        console.warn('-----------************',this.state.structure);
        this.getStructureApi();
    }
    componentDidUpdate=(prevProps,prevState)=>{
        if ((prevProps.host != this.props.host) || (prevProps.microservice != this.props.microservice) || (prevProps.model != this.props.model)){
            this.setState({structure:{}});
            //this.getStructureApi();
        }
    };

    getUrlAPI(_tipo){
        return getUrlApi(_tipo,this.props);
    }
    getData(){
        if (!!this.props.rawData){
            return this.setState({
                structure:this.mapToDataCreated(this.props.rawData,this.state.structure),
                loadingData:false
            });
        }
        if (!this.props.id){
            this.setState({loadingData:false});
            return false
        }
        axios.get(this.getUrlAPI()+'/'+this.props.id).then((res)=>{
            //console.log('data obtenida',res);
            this.setState({loadingData:false,structure:this.mapToDataCreated(res.data,this.state.structure)});
        }).catch((err)=>{
            this.setState({loadingData:false});
            console.log('find',err);
        });
    }

    mapToDataCreated(data,structure){
        if (!data || !structure) {return false};
        var newStructure={};
        for (var value in structure){
            newStructure[value]=Object.assign(structure[value],{}) ;
            for (var val in data){
                if (val==value){
                    if (!!data[val] && data[val] instanceof Object){
                        newStructure[value].value= data[val][newStructure[value].valuekey];
                    }else{
                        newStructure[value].value= data[val];
                    }
                }
            }
        }
        return newStructure;
    }
    getStructureApi(){
        this.setState({loadingStructure:true});
        axios.get(this.getUrlAPI('structure')).then(({data})=>{
            this.setState(
                {
                    structure:this.props.transformStructure(this.omitArraysInStructure(JSON.parse(data.structure))),
                    loadingData:true,
                    loadingStructure:false
                },()=>this.getData());
        }).catch((err)=>{
            this.setState({loadingStructure:false,loadingData:false});
            console.error('error',err);
        });
    }
    onChange=(e)=>{
        this.setState({structure:e.structure});
        // console.log('ESTRUCTURA',e);
        this.props.onChange(e);
    }
    render () {        
        return (
            <div>                
                {this.state.loadingStructure && <div>Loading Structure</div>}
                {this.state.loadingData && <div>Loading Data</div>}
                {(!!this.state.loadingData || !!this.state.loadingStructure)
                    &&
                    <div>
                        <div className="stk-Forms-loading"> <BarLoader width="100"/></div>        
                        <MockForm/>
                    </div>
                }
                {(!!this.state.structure && !this.state.loadingData) ?
                    <GenericForm
                        structure={this.state.structure}                    
                        showButtons={false}
                        value={this.props.value}
                        host={this.props.host}
                        readOnly={this.props.readOnly}
                        editingId={this.props.id}
                        onChange={this.onChange}
                        disabledButtonWhenUploading={this.props.disabledButtonWhenUploading}
                        axiosInstance={this.props.axiosInstance}
                    />
                    :null
                }
            </div>

        );
    }
}

GenericFormApi.propTypes = {
    host: PropTypes.string,
    model: PropTypes.string,
    value:PropTypes.object,
    microservice: PropTypes.string,
    viaRelation:PropTypes.string,
    viaValue:PropTypes.string,
    transformStructure:PropTypes.func,
    readOnly:PropTypes.bool
};
GenericFormApi.defaultProps = {
    readOnly:false,
    onChange:()=>{},
    transformStructure:(_structure)=>_structure
};
const MockForm=()=>
    (<div style={{marginTop:'1.5em'}}>
        {[1,2,3].map((_val,_i)=>
        <div className="row"
             style={{marginBottom:'1em'}}
             key={_i}>
            {[1,2].map((_val,_ic)=>
                <div className="col" key={_ic} >
                    <div>--------------</div>
                    <div className="progress progress-lg progress-bar-striped">
                        <div className="progress-bar progress-bar-default"
                             role="progressbar" style={{width: "12%"}} aria-valuenow="12" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            )}
        </div>
        )}
    </div>)

export default GenericFormApi;
/**
 * Created by estratek on 15/12/17.
 */
var React = require('react');
//import {Modal} from 'reactstrap';
const Modal = require('reactstrap').Modal;
import PropTypes from 'prop-types';

class DynamicInputTableConfigStructure extends React.Component{
    state={
        structure:{}
    };
    componentWillUpdate(nextProps){
        if (nextProps!=this.props){
            this.setState({structure:this.addHiddenProp(this.props.structure)})
        }
    }

    addHiddenProp(structure){
        var array={};
        for (var value in structure){
            array[value]=structure[value];
            array[value].hidden=structure[value].hidden || false;
        }
        return array;
    }

    handleChange(e,key) {
        var array = this.state.structure;
        array[key].hidden = !e.target.checked;
        this.setState({structure:array});
    }
    save=(_notSave)=>{
        if (!!this.props.onSave){
            if (_notSave){
                return this.props.onSave(this.props.structure);
            }
            this.props.onSave(this.state.structure);
        }
    }
    render(){
        return (
            <Modal isOpen={this.props.isOpen}>
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Ver/ocultar campos</h5>
                        <button type="button" className="close" onClick={(e)=>this.save(false)}>
                            <span >&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        {!!this.props.structure &&
                            <table className="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">Columna</th>
                                <th scope="col">Mostrar</th>
                            </tr>
                            </thead>
                            <tbody>
                            {Object.keys(this.state.structure)
                            .filter(x=>!Array.isArray(this.state.structure[x]))
                            .filter(x=>x!='_selfTablePermissions').map((value,index)=>
                                <tr key={index}>
                                    <td>{value}</td>
                                    <td>
                                        <input type="checkbox"
                                               onChange={(e)=>this.handleChange(e,value)}
                                               checked={!this.state.structure[value].hidden}/>
                                    </td>
                                </tr>
                            )}
                            </tbody>
                        </table>
                        }
                    </div>
                    <div className="modal-footer">
                        <button className="btn btn-primary" onClick={this.save}>Guardar</button>
                    </div>
                </div>
            </Modal>
        );
    }
}



DynamicInputTableConfigStructure.propTypes = {
    onSave:PropTypes.func.isRequired
};
DynamicInputTableConfigStructure.defaultProps = {};

export default DynamicInputTableConfigStructure;
/**
 * Created by estratek on 15/12/17.
 */
var React = require('react');
import GfsValidator, {getArrayErrors} from './GfsValidator';
import UploadMyFile from './StkUploadFile';
import GfsOnlyView from './GfsOnlyView';

export default class GfsInputFileImage extends React.Component {
    state = {
        imageWasLoaded: false,
    }

    componentWillUpdate(nextProps){
        if (nextProps.value!==this.props.value && !!this.props.onChangeError){
            this.props.onChangeError(getArrayErrors(nextProps.validation,nextProps.value).length>0);
        }
    }
    componentDidMount(){
        if (!this.props.onChangeError) {return false}
        this.props.onChangeError(this.hasError());
    }
    onUploadFile(file){
        console.log('when upload a new file')
        this.props.onChange(file.data.url);
    }
    hasError(){
        if (!this.props.validation) return false;
        return getArrayErrors(this.props.validation,this.props.value).length>0;
    }

    onLoadImage = () => {
        !!this.props.disabledButtonWhenUploading && this.props.disabledButtonWhenUploading(false)
        console.log('value =>>>>>>>>>', this.props.value)
        this.setState(() => ({ imageWasLoaded: true }))
    }

    renderIMG = (flag) => {
        !!this.props.disabledButtonWhenUploading && this.props.disabledButtonWhenUploading(!flag)
        this.setState(() => ({ imageWasLoaded: flag }))
    }

    disabledButtonWhenUploading = (flag) => {
        !!this.props.disabledButtonWhenUploading && this.props.disabledButtonWhenUploading(flag)
    }

    render () {
        console.log('this.state', this.state);
        let isDoc = false;
        if (this.props.value) {
            let last3Letters = this.props.value.substr(this.props.value.length - 3)
            let imgArr = ['png', 'PNG', 'jpg', 'peg', 'JPG']
            isDoc = !imgArr.includes(last3Letters)
        }
        if (!!this.props.readOnly){
            return (<GfsOnlyView label={this.props.label} component={(!!this.props.value)? (<a className={"value"}
            style={{ textDecoration: "underline" }} href={this.props.value} download>Link</a>):<p>----</p>  }/>)
        }
        return (
            <div className={"form-group "+((this.hasError())?'has-error':'')}>
                <div className="container">
                        <div className="row">
                            <div className="col-md-5 text-right">
                                <div style={{height:'10em',width:'10em',overflow:'hidden',display:'inline-block'}} >
                                    {!!this.props.value  ?
                                        (isDoc ? <i style={{ padding: '15px', fontSize: '7em' }} className="fa fa-file-text fa-5x" /> : <img onLoad={this.onLoadImage} className="img-circle" src={this.props.value} style={{maxWidth:'100%',maxHeight:'100%',height:'100%'}} alt=""/>)
                                        :
                                       <img className="img-circle" src="https://s3.amazonaws.com/estratek-persona/1517250727463.png"
                                             style={{maxWidth:'100%',maxHeight:'100%',height:'100%'}}
                                             onLoad={this.onLoadImage}
                                             alt=""/>
                                    }
                                </div>
                            </div>

                            <div className="col-md-7 text-left">
                                <div className="p-t-40">
                                    <UploadMyFile 
                                        onUploaded={this.onUploadFile.bind(this)}
                                        urlFileUploaded={this.props.fileUploaded}
                                        elementToShow={<button className="btn btn-default p-l-20 p-r-20">{this.props.placeholder || 'Subir Foto'}</button>} 
                                        renderIMG={this.renderIMG}
                                        flagOfRenderIMG={this.state.imageWasLoaded || isDoc}
                                    />

                                        <GfsValidator validation={this.props.validation}
                                                      value={this.props.value}/>
                                        <p id="emailHelp" className="text-muted p-t-10">{this.props.description}</p>
                                    </div>
                            </div>
                        </div>

                </div>

            </div>
        );
    }
}
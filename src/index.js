/**
 * Created by estratek on 14/03/18.
 */
import DynamicModal from './GenericForm/DynamicModal'
import DynamicSelect from './GenericForm/DynamicSelect'
import GenericCrud from './GenericForm/GenericCrud/GenericCrud'
import GenericForm from './GenericForm/GenericForm'
import GenericFormApi from './GenericForm/GenericFormApi'
import GfsInput from './GenericForm/GfsInput'
import GfsInputDatePicker from './GenericForm/GfsInputDatePicker'
import GfsInputTime from './GenericForm/GfsInputTime'
import GfsInputFileImage from './GenericForm/GfsInputFileImage'
import GfsInputTextArea from './GenericForm/GfsInputTextArea'
import GfsValidator from './GenericForm/GfsValidator'
import StkUploadFile from './GenericForm/StkUploadFile'
import GList from './GenericForm/GList/GList'
import ModalError from './GenericForm/ModalError'
import {getCookie} from './cookieDriver'
import 'react-datepicker/dist/react-datepicker-cssmodules.min.css';
import 'react-select-plus/dist/react-select-plus.min.css';
import './custom.css';
var Modal = require('reactstrap').Modal;
const axios =require('axios');

axios.interceptors.request.use(function (config) {
    // Do something before request is sent
    //console.log('en mi component',config);
    if (!!getCookie('session_persona')){
        let storage_user=JSON.parse(decodeURIComponent(getCookie('session_persona')));
        config.headers['apikey']=storage_user.key;
    }
    if (!!getCookie('workspace')){
        let storage_user=JSON.parse(decodeURIComponent(getCookie('workspace')));
        //console.log('WOOORSKPACE',storage_user);
        config.headers['wp']=storage_user.domain;
    }
    return { ...config, withCredentials: true};
}, function (error) {
    // Do something with request error
    
    return Promise.reject(error);
});

  

export {
    DynamicModal,
    DynamicSelect,
    GenericCrud,
    GenericForm,
    GenericFormApi,
    GfsInput,
    GfsInputDatePicker,
    GfsInputFileImage,
    GfsInputTextArea,
    GfsValidator,
    StkUploadFile,
    GList,
    ModalError,
    GfsInputTime
}